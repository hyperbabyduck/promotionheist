// Copyright (C) 2015-2019 ricimi - All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement.
// A Copy of the Asset Store EULA is available at http://unity3d.com/company/legal/as_terms.

using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Ricimi
{
    /// <summary>
    /// Basic button class used throughout the demo.
    /// </summary>
    public class BasicButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public float fadeTime = 0.2f;
        public float onHoverAlpha;
        public float onClickAlpha;
        [SerializeField] float deactivatedAlpha;
        [SerializeField] bool isAction;
        [SerializeField] bool isEnd;

        Unit currentUnit;

        [Serializable]
        public class ButtonClickedEvent : UnityEvent { }

        [SerializeField]
        private ButtonClickedEvent onClicked = new ButtonClickedEvent();

        private CanvasGroup canvasGroup;

        private void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();
        }

        public void SetUnit(Unit unit)
		{
            currentUnit = unit;
            SetActivatedButtons();
		}
		public void SetActivatedButtons()
		{
			if(!CanBeSelected())
			{
                canvasGroup.alpha = deactivatedAlpha;
			}

		}

		public virtual void OnPointerEnter(PointerEventData eventData)
        {
            if (CanBeSelected())
            {
                if (eventData.button != PointerEventData.InputButton.Left)
                {
                    return;
                }

                StopAllCoroutines();
                StartCoroutine(Utils.FadeOut(canvasGroup, onHoverAlpha, fadeTime));
            }
            else
			{
                canvasGroup.alpha = deactivatedAlpha;
			}
        }

        public virtual void OnPointerExit(PointerEventData eventData)
        {
            if (CanBeSelected())
            {
                if (eventData.button != PointerEventData.InputButton.Left)
                {
                    return;
                }

                StopAllCoroutines();
                StartCoroutine(Utils.FadeIn(canvasGroup, 1.0f, fadeTime));
            }
        }

        public void ResetButton()
		{
            GetComponent<CanvasGroup>().alpha = 1;
            GetComponent<Tooltip>().tooltip.GetComponent<CanvasGroup>().alpha = 0;
        }

        public void DisableButton()
		{
            GetComponent<CanvasGroup>().alpha = deactivatedAlpha;
            GetComponent<Tooltip>().tooltip.GetComponent<CanvasGroup>().alpha = 0;
        }

        private bool CanBeSelected()
		{
            bool value;

            if (isAction)
			{
                if (isEnd)
				{
                    value = true;
				}
				else
				{
                    value = currentUnit.CanAct;
				}

			}
			else
			{
                value = currentUnit.CanMove;

            }

            return value;
		}
    }
}