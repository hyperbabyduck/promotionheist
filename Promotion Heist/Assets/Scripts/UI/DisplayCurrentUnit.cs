﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayCurrentUnit : MonoBehaviour
{
	[SerializeField] Text unitDisplayText;
	[SerializeField] Text inputDisplayText;
	[SerializeField] Text phaseDisplayText;

	public void SetUnitText(string value)
	{
		unitDisplayText.text = value;
	}

	public void SetInputText(string value)
	{

		inputDisplayText.text = value;
	}

	public void SetPhaseText(string value)
	{
		phaseDisplayText.text = value;
	}
	
}
