﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayUnitStats : MonoBehaviour
{
	[SerializeField] Image unitPicture;
	[SerializeField] Image colourBackground;
	[SerializeField] Text nameText;
	[SerializeField] Text witText;
	[SerializeField] Text selfEsteemText;
	[SerializeField] Text persuasionText;
	[SerializeField] Text energyText;
	[SerializeField] Image energyBar;
	[SerializeField] Image[] notorietyStars;
	[SerializeField] Text[] textArray;
	[SerializeField] Text[] textBackgroundArray;
	[SerializeField] Color blueTeamColour;
	[SerializeField] Color redTeamColour;
	[SerializeField] Color yellowTeamColour;

	Unit currentUnit;

	public Unit CurrentUnit
	{
		get { return currentUnit; }
		set 
		{ 
			currentUnit = value;
			WipeUnitDetails();
			SetCurrentUnit();
		}
	}

	public void ShowStats(bool value)
	{
		gameObject.SetActive(value);
	}

	private void SetCurrentUnit()
	{
		if (CurrentUnit != null)
		{
			unitPicture.sprite = CurrentUnit.Stats.Picture;
			unitPicture.enabled = true;
			nameText.text = CurrentUnit.Stats.Name;
			witText.text = CurrentUnit.Stats.Wit.ToString();
			selfEsteemText.text = CurrentUnit.Stats.SelfEsteem.ToString();
			persuasionText.text = CurrentUnit.Stats.Persuasion.ToString();
			energyText.text = Mathf.Round(CurrentUnit.Stats.Energy).ToString();
			energyBar.fillAmount = Mathf.Round(CurrentUnit.Stats.Energy) / 100;
			SetNotorietyStars(CurrentUnit.Stats.Notoriety);

			switch(currentUnit.Stats.Team)
			{
				case GamePhase.BlueTeam:
					colourBackground.color = blueTeamColour;
					break;

				case GamePhase.RedTeam:
					colourBackground.color = redTeamColour;
					break;

				case GamePhase.YellowTeam:
					colourBackground.color = yellowTeamColour;
					break;
			}
		}
		else
		{
			WipeUnitDetails();
		}
	}

	private void WipeUnitDetails()
	{
		unitPicture.enabled = false;
		nameText.text = "";
		witText.text = "";
		selfEsteemText.text = "";
		persuasionText.text = "";
		energyText.text = "";
		energyBar.fillAmount = 0;
		SetNotorietyStars(0);
		colourBackground.color = Color.white;
	}

	public void SetNotorietyStars(int value)
	{
		for (int index = 0; index < notorietyStars.Length; index += 1)
		{
			if (index + 1 <= value)
			{
				notorietyStars[index].enabled = true;
			}
			else
			{
				notorietyStars[index].enabled = false;
			}
		}
	}

	public void ShowText(string value, int index, Color colour)
	{
		textArray[index].color = colour;
		textArray[index].text = value;
		textBackgroundArray[index].text = value;
	}

	public void WipeText()
	{
		foreach (Text text in textArray)
		{
			text.color = Color.white;
			text.text = " ";
			
		}

		foreach (Text text in textBackgroundArray)
		{
			text.text = " ";
		}
	}

	public void UpdateStats()
	{
		if (currentUnit)
		{
			witText.text = CurrentUnit.Stats.Wit.ToString();
			selfEsteemText.text = CurrentUnit.Stats.SelfEsteem.ToString();
			persuasionText.text = CurrentUnit.Stats.Persuasion.ToString();
			energyText.text = Mathf.Round(CurrentUnit.Stats.Energy).ToString();
			energyBar.fillAmount = Mathf.Round(CurrentUnit.Stats.Energy) / 100;
			SetNotorietyStars(CurrentUnit.Stats.Notoriety);
		}
	}
}
