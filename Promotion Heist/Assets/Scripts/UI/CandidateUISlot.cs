﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CandidateUISlot : MonoBehaviour
{
	[SerializeField] Text nameText;
	[SerializeField] Text promotionChanceText;
	[SerializeField] Image promotionBar;
	[SerializeField] Image portrait;

	Unit candidate;

	public void SetCandidate(Unit unit)
	{
		candidate = unit;
		nameText.text = candidate.Stats.Name;
		portrait.sprite = candidate.Stats.Headshot;

		gameObject.SetActive(true);
	}

	private void Update()
	{
		if (candidate != null)
		{
			promotionChanceText.text = candidate.Stats.PromotionChance.ToString() + "%";
			promotionBar.fillAmount = candidate.Stats.PromotionChance / 100;
		}
	}
}
