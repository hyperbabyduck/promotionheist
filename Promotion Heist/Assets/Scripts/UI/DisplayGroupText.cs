﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayGroupText : MonoBehaviour
{
    [SerializeField] Text candidateText;
    [SerializeField] Text candidateValue;

    public void SetCandidateText(string value)
    {
        candidateText.text = value;
	}

    public void SetCandidateValue(string value)
    {
        candidateValue.text = value;
    }
}
