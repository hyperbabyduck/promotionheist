﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextSpawner : MonoBehaviour
{
	[SerializeField] GameObject textPrefab;
	[SerializeField] Transform candidateText;

	public void SpawnText(string value, Color colour)
	{
		Transform transform = candidateText;
		
		GameObject spawnedText = Instantiate(textPrefab, transform);
		spawnedText.GetComponent<DisplayText>().ForegroundText().color = colour;
		spawnedText.GetComponent<DisplayText>().ForegroundText().text = value;
		spawnedText.GetComponent<DisplayText>().BackgroundText().text = value;
	}
}
