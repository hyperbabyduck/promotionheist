﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayGamePhase : MonoBehaviour
{
    [SerializeField] Text phaseText;
    [SerializeField] Text phaseTextBackground;
    [SerializeField] Image phaseBackground;
    [SerializeField] Color blueTeamColour;
    [SerializeField] Color redTeamColour;
    [SerializeField] Color yellowTeamColour;
    [SerializeField] Color bossColour;

    bool isShowing;
    GamePhase newPhase;

    public void SetNewPhase(GamePhase phase)
	{
        newPhase = phase;

        //if is showing
        if (isShowing)
		{
            HidePhaseUI();
		}
		else
		{
            SetPhaseUI();
            GetComponent<Animator>().SetTrigger("Open");
            isShowing = true;
        }
	}

	private void HidePhaseUI()
	{
        //hide popup
        GetComponent<Animator>().SetTrigger("Close");
        isShowing = false;
	}

    public void UIClosed()
    {
        SetPhaseUI();
        GetComponent<Animator>().SetTrigger("Open");
        isShowing = true;
    }

    private void SetPhaseUI()
    {
        switch (newPhase)
		{
            case GamePhase.BlueTeam:
                //set colour
                phaseBackground.color = blueTeamColour;
                phaseText.text = "BLUE TEAM'S TURN";
                phaseTextBackground.text = "BLUE TEAM'S TURN";
                break;

            case GamePhase.RedTeam:
                phaseBackground.color = redTeamColour;
                phaseText.text = "RED TEAM'S TURN";
                phaseTextBackground.text = "RED TEAM'S TURN";
                break;

            case GamePhase.YellowTeam:
                phaseBackground.color = yellowTeamColour;
                phaseText.text = "YELLOW TEAM'S TURN";
                phaseTextBackground.text = "YELLOW TEAM'S TURN";
                break;

            case GamePhase.Boss:
                phaseBackground.color = bossColour;
                phaseText.text = "BOSS' TURN";
                phaseTextBackground.text = "BOSS' TURN";
                break;

        }
    }

    //leave blank
    public void UIOpened()
    {

    }
}
