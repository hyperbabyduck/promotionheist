﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayText : MonoBehaviour
{
	[SerializeField] Text foregroundText;
	[SerializeField] Text backgroundText;
	[SerializeField] float destroyTime;

	public Text ForegroundText()
	{
		return foregroundText;
	}

	public Text BackgroundText()
	{
		return backgroundText;
	}

	private void Start()
	{
		StartCoroutine(DestroySelf());
	}

	private IEnumerator DestroySelf()
	{
		yield return new WaitForSeconds(destroyTime);

		Destroy(gameObject);
	}
}
