﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIFader : MonoBehaviour
{
    CanvasGroup canvasGroup;

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    public void FadeOutImmediate()
    {
        canvasGroup.alpha = 0;
    }

    public void FadeOut(float time)
    {
        StartCoroutine(Fade(0, time));
    }

    public void FadeIn(float time)
    {
        StartCoroutine(Fade(1, time));
    }

    private IEnumerator Fade(float target, float time)
    {
        while (!Mathf.Approximately(canvasGroup.alpha, target))
        {
            canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, target, Time.deltaTime / time);

            if (canvasGroup.alpha >= 1)
            {
                canvasGroup.blocksRaycasts = true;
            }
            else
            {
                canvasGroup.blocksRaycasts = false;
            }
            yield return null;
        }
    }
}
