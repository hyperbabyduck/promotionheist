﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandidateUIDisplayer : MonoBehaviour
{
    [SerializeField] CandidateUISlot[] candidateSlots;
    [SerializeField] UnitController unitController;

	public void InitializeSlots()
	{
		//get all candidates
        for(int index = 0; index < unitController.GetList(GamePhase.Candidate).Count; index += 1)
		{
            Unit candidate = unitController.GetList(GamePhase.Candidate)[index];

            candidateSlots[index].SetCandidate(candidate);
        }

	}
}
