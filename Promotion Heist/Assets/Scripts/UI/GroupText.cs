﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GroupText : MonoBehaviour
{
    [SerializeField] Text nameText;
    [SerializeField] Image portrait;
    [SerializeField] Text[] textArray;
    [SerializeField] Text[] textBackgroundArray;
    [SerializeField] DisplayGroupText[] candidateTexts;
    Group currentGroup;

    public Group CurrentGroup
    {
        get { return currentGroup; }
        set
        {
            currentGroup = value;
            WipeGroupDetails();

            if (value != null)
			{
                SetCurrentGroup();
            }
        }
    }

    private void WipeGroupDetails()
    {
        nameText.text = " ";
        portrait.enabled = false;
        portrait.sprite = null;

        for (int index = 0; index < candidateTexts.Length; index += 1)
        {
            candidateTexts[index].SetCandidateText(" ");
            candidateTexts[index].SetCandidateValue(" ");
        }
    }

    private void SetCurrentGroup()
    {
        nameText.text = currentGroup.GroupName;
        portrait.sprite = currentGroup.ProfileSprite();
        portrait.enabled = true;

        for (int index = 0; index < candidateTexts.Length; index += 1)
        {
            candidateTexts[index].SetCandidateText(currentGroup.candidateStats[index].candidate.Stats.Name);
            candidateTexts[index].SetCandidateValue(currentGroup.candidateStats[index].modifier.ToString());
        }
    }

    public void SetNameText(string value)
    {
        nameText.text = value;
    }

    public void ShowText(string value, int index, Color colour)
    {
        textArray[index].color = colour;
        textArray[index].text = value;
        textBackgroundArray[index].text = value;
    }

    public void WipeText()
    {
        foreach(Text text in textArray)
		{
            text.color = Color.white;
            text.text = " ";
		}

        foreach (Text text in textBackgroundArray)
        {
            text.text = " ";
        }
    }
}

