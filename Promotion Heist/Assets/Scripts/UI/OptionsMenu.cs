﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsMenu : MonoBehaviour
{
	[SerializeField] GameObject menu;
	[SerializeField] GameObject blackScreen;
	[SerializeField] BattleController battleController;
	[SerializeField] InputController inputController;

	InputState storedInputState = InputState.None;

	public void ResumeLevel()
	{
		SetMenu();
	}

	public void RestartLevel()
	{
		FindObjectOfType<LevelLoader>().LoadLevel();
		PlayClick();
	}

	public void ReturnToMainMenu()
	{
		FindObjectOfType<LevelLoader>().LoadMainMenu();
		PlayClick();
	}

	public void QuitApplication()
	{
		PlayClick();
		Application.Quit();
	}

	public void SetMenu()
	{
		if (menu.activeSelf == true)
		{
			menu.SetActive(false);

			if (battleController.Phase != GamePhase.Boss)
			{
				if (storedInputState != InputState.None)
				{
					battleController.GetComponent<InputController>().InputMode = storedInputState;
				}
				else
				{
					inputController.InputMode = InputState.SelectUnit;
				}
				
			}
		}
		else
		{
			if (battleController.Phase != GamePhase.Boss)
			{
				inputController.InputMode = storedInputState;
			}

			menu.SetActive(true);
		}

		PlayClick();

	}

	public void SetBlackScreen(bool value)
	{
		if (blackScreen.activeSelf == value)
		{
			blackScreen.SetActive(!value);
		}
		else
		{
			blackScreen.SetActive(value);
		}
		
	}

	private void Update()
	{
		if (menu.activeSelf == true)
		{
			inputController.InputMode = InputState.None;
		}
	}

	public void PlayClick()
	{
		GetComponent<AudioSource>().Play();
	}
}
