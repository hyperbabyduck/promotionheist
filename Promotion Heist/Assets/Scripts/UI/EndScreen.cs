﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScreen : MonoBehaviour
{
	[SerializeField] Text title;
	[SerializeField] Text description;
	[TextArea(2, 5)]
	[SerializeField] string winTitle;
	[TextArea(2, 5)]
	[SerializeField] string winnerText;

	public void SetText(GamePhase team)
	{
		string teamName = "";

		switch (team)
		{
			case GamePhase.BlueTeam:
				teamName = "The Blue Team";
				break;

			case GamePhase.RedTeam:
				teamName = "The Red Team";
				break;

			case GamePhase.YellowTeam:
				teamName = "The Yellow Team";
				break;
		}

		title.text = teamName + " " + winTitle;
		description.text = teamName + " " + winnerText;
		gameObject.SetActive(true);
	}

	public void RestartLevel()
	{
		FindObjectOfType<LevelLoader>().RestartLevel();
	}

	public void ReturnToMenu()
	{
		FindObjectOfType<LevelLoader>().LoadMainMenu();
	}

	public void QuitApplication()
	{
		Application.Quit();
	}
}
