﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayTurnLimit : MonoBehaviour
{
    [SerializeField] Text roundValue;
    [SerializeField] Text maxRoundValue;  
    [SerializeField] Text roundValueBack;
    [SerializeField] Text maxRoundValueBack;
    [SerializeField] GameObject turnUI;

    public void ShowUI()
	{
        turnUI.SetActive(true);
	}

    public void SetMaxRoundValue(int value)
	{
        maxRoundValue.text = value.ToString();
        maxRoundValueBack.text = value.ToString();
    }

    public void SetRoundValue(int value)
    {
        roundValue.text = value.ToString();
        roundValueBack.text = value.ToString();
    }
}
