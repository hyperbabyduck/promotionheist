﻿using Ricimi;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionButton : MonoBehaviour
{
	AbilityStats ability;

	public bool InitializeButton(AbilityStats ability)
	{
		Ability = ability;

		GetComponent<Tooltip>().SetTitleText(ability.ability.GetName());

		if (ability.currentCooldown == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public AbilityStats Ability
	{
		get { return ability; }
		set { ability = value; }
	}

	public void PerformAction()
	{
		GameObject.FindGameObjectWithTag("UIController").GetComponent<UIController>().StartAction(ability);
	}
}
