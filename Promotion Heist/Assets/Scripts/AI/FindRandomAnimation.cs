﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindRandomAnimation : MonoBehaviour
{ 

	[SerializeField] AnimationType[] animations;
	[SerializeField] float waitTimeMin = 0;
	[SerializeField] float waitTimeMax = 10f;
	bool canAnimate = true;
	float waitTime;

	private void Start()
	{
		SetAnimation();
	}

	private void SetAnimation()
	{
		waitTime = Random.Range(0, 5f);

		StartCoroutine(CountdownToAnimation());
	}	

	private void ResetAnimation()
	{
		waitTime = FindNewWaitTime();
		StartCoroutine(CountdownToAnimation());
	}

	//find wait time
	private float FindNewWaitTime()
	{
		return Random.Range(waitTimeMin, waitTimeMax);
	}

	//IEnumerator for countdown
	private IEnumerator CountdownToAnimation()
	{
		yield return new WaitForSeconds(waitTime);

		if (CanAnimate)
		{
			PlayAnimation(FindAnimation());
		}

		ResetAnimation();
	}

	//start animation
	private void PlayAnimation(string index)
	{
		GetComponentInChildren<Animator>().SetTrigger(index);
	}

	//find animation 
	private string FindAnimation()
	{
		AnimationType index = animations[Random.Range(0, animations.Length)];

		switch (index)
		{
			case AnimationType.Talk1:

				return "Talk1";

			case AnimationType.Talk2:

				return "Talk2";

			case AnimationType.Talk3:

				return "Talk3";

			case AnimationType.Talk4:

				return "Talk4";

			case AnimationType.Talk5:

				return "Talk5";

			case AnimationType.Talk6:

				return "Talk6";

			case AnimationType.Talk7:

				return "Talk7";

			case AnimationType.Talk8:

				return "Talk8";

			default:
				return null;


		}
	}

	public bool CanAnimate
	{
		get { return canAnimate; }
		set { canAnimate = value; }
	}
}
