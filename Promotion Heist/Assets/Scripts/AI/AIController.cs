﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AIController : MonoBehaviour
{
	[SerializeField] float waitTime = 2f;

	struct NodeDistance
	{
		public Node node;
		public float distance;
	}

	GridController gridController;
	UnitController unitController;

	Group targetGroup = null;
	
	List<Node> moveNodes = new List<Node>();

	private void Start()
	{
		gridController = GameObject.FindGameObjectWithTag("GridController").GetComponent<GridController>();
		unitController = GameObject.FindGameObjectWithTag("BattleController").GetComponent<UnitController>();
	}

	public void FindAction(Unit currentUnit)
	{
		targetGroup = null;

		//Get action
		AbilityStats currentAbility = currentUnit.Stats.Abilities[0];

		//set move nodes
		SetMoveNodes(currentUnit);

		if (moveNodes.Count <= 0)
		{
			StartCoroutine(currentUnit.WaitAndEndTurn());
		}
		else
		{
			Node foundNode = null;

			FindTargetGroup(currentUnit);

			if (targetGroup != null)
			{
				//add neighbour nodes to list if they are move nodes
				List<Node> targetNeighbours = new List<Node>();
				FindTargetNeighbours(targetNeighbours);

				//if there are any neighbours that are move nodes
				if (targetNeighbours.Count > 0)
				{
					//find nearest node 
					foundNode = FindNearestGroupNode(currentUnit, targetNeighbours);
					print("group found");
					//perform action
					StartCoroutine(StartMovement(currentUnit, true, foundNode, currentAbility));
				}
				else
				{
					//find closest move node to group
					foundNode = FindNearestMoveNode();

					//start movement
					StartCoroutine(StartMovement(currentUnit, false, foundNode, currentAbility));
				}
			}
			else
			{
				StartCoroutine(StartMovement(currentUnit, false, gridController.GetNodeFromGrid(currentUnit.GridPos), currentAbility));
			}
		}
	}

	private void FindTargetNeighbours(List<Node> targetNeighbours)
	{
		//search through neighbours
		foreach (Vector2Int direction in gridController.GetDirections())
		{
			if (gridController.GridContainsNode(targetGroup.GridPos + direction))
			{
				Node currentNode = gridController.GetNodeFromGrid(targetGroup.GridPos + direction);

				//if the node isnt occupied
				if (!currentNode.IsOccupied && currentNode.IsMoveNode)
				{
					targetNeighbours.Add(currentNode);
				}
			}
		}
	}

	private Node FindNearestMoveNode()
	{
		List<NodeDistance> nodeDistances = new List<NodeDistance>();

		foreach(Node moveNode in moveNodes)
		{
			NodeDistance newNodeDistance = new NodeDistance();
			newNodeDistance.node = moveNode;
			newNodeDistance.distance = Vector3.Distance(targetGroup.transform.position, moveNode.transform.position);
			nodeDistances.Add(newNodeDistance);
		}

		//find first node
		NodeDistance selectedNode = nodeDistances[0];

		foreach(NodeDistance nodeDistance in nodeDistances)
		{
			if (nodeDistance.node != selectedNode.node)
			{
				if (IsHigherThan(nodeDistance.distance, selectedNode.distance))
				{
					selectedNode = nodeDistance;
				}
			}
		}

		//set closest node as found node
		return selectedNode.node;
	}

	private Node FindNearestGroupNode(Unit currentUnit, List<Node> targetNeighbours)
	{
		List<NodeDistance> nodeDistances = new List<NodeDistance>();

		foreach (Node node in targetNeighbours)
		{
			NodeDistance newNodeDistance = new NodeDistance();
			newNodeDistance.node = node;
			newNodeDistance.distance = Vector3.Distance(currentUnit.transform.position, node.transform.position);
			nodeDistances.Add(newNodeDistance);
		}

		//find first node
		NodeDistance selectedNode = nodeDistances[0];

		foreach (NodeDistance nodeDistance in nodeDistances)
		{
			if (nodeDistance.node != selectedNode.node)
			{
				if (IsHigherThan(selectedNode.distance, nodeDistance.distance))
				{
					selectedNode = nodeDistance;
				}
			}
		}

		//set closest node as found node
		return selectedNode.node;
	}

	private void FindTargetGroup(Unit currentUnit)
	{
		//find the target group
		targetGroup = unitController.GetGroupList()[currentUnit.BossGroupIndex];
	}

	private void SetMoveNodes(Unit currentUnit)
	{
		moveNodes.Clear();
		gridController.FindAIMoveNodes(currentUnit);
	}

	public void AddToMoveNodeList(Node node)
	{
		moveNodes.Add(node);
	}

	private IEnumerator StartMovement(Unit currentUnit, bool hasFollowUpAction, Node selectedNode, AbilityStats ability)
	{
		//if the unit needs to move to their node
		if (selectedNode != gridController.GetNodeFromGrid(currentUnit.GridPos))
		{
			//show move nodes
			gridController.ColourNodes();

			//wait seconds to show move nodes
			yield return new WaitForSeconds(waitTime);

			//hide move nodes
			gridController.ClearNodeColour();

			//move along path
			yield return GetComponent<Mover>().AIFollowPath(gridController.GetPath(gridController.GetNodeFromGrid(currentUnit.GridPos), selectedNode), currentUnit);
		}

		//if follow up action
		if (hasFollowUpAction)
		{
			StartCoroutine(StartAction(currentUnit, gridController.GetNodeFromGrid(targetGroup.GridPos), ability));
		}
		else
		{
			gridController.WipeNodes();

			StartCoroutine(currentUnit.WaitAndEndTurn());
		}
	}

	private IEnumerator StartAction(Unit currentUnit, Node selectedNode, AbilityStats ability)
	{
		//clear move nodes
		gridController.WipeMoveNodes();

		//highlight groups node
		gridController.GetNodeFromGrid(targetGroup.GridPos).Highlighted = true;

		//show groups UI
		GameObject.FindGameObjectWithTag("UIController").GetComponent<UIController>().SetHighlightedGroup(targetGroup);

		//colour nodes
		gridController.ColourNodes();

		//set the unit's selected ability
		currentUnit.SelectedAbility = ability;

		//wait for seconds
		yield return new WaitForSeconds(waitTime);

		//perform action
		currentUnit.PerformAction(selectedNode);
	}

	private bool IsHigherThan(float x, float y)
	{
		return x < y;
	}
}

