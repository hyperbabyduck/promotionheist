﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
	[SerializeField] float waitTime = 2f;
	[SerializeField] DirectionFacing startingDirection;
	[SerializeField] GameObject endTurnIcon;
	[SerializeField] GameObject stunnedIcon;
	[SerializeField] Color positiveColour;
	[SerializeField] Color negativeColour;
	[SerializeField] Transform fxSpawnPoint;
	[SerializeField] GameObject talkingGraphic;

	//Unit Stats
	StatsController statsController;
	AIController aiController;

	//Turn Stats
	int actions = 0;
	bool canMove = false;
	bool canAct = false;
	bool hasTakenTurn = false;
	int bossGroupIndex;
	Unit previousUnit;
	Group previousGroup;
	AbilityStats selectedAbility;
	AbilityStats usedAbility;

	bool isMoving = false;
	DirectionFacing directionFacing = DirectionFacing.Up;
	Node previousNode;

	InputController inputController;
	AbilityController abilityController;
	Mover myMover;

	public Vector2Int gridPos = new Vector2Int(0, 0);

	private void Awake()
	{
		Stats = GetComponent<StatsController>();
		Stats.InitializeCharacter(); 

		if (Stats.Team == GamePhase.Boss)
		{
			aiController = GetComponent<AIController>();
		}
		else
		{ 
			Destroy(GetComponent<AIController>());
		}
	}

	private void Start()
	{
		inputController = GameObject.FindGameObjectWithTag("InputController").GetComponent<InputController>();
		abilityController = FindObjectOfType<AbilityController>();
		SetStartingDirection();

		myMover = GetComponent<Mover>();
	}

	//SETUP
	private void SetStartingDirection()
	{
		float rotation = 0;
		switch (startingDirection)
		{
			case DirectionFacing.Up:
				rotation = 0;
				break;

			case DirectionFacing.Right:
				rotation = 90f;
				break;

			case DirectionFacing.Down:
				rotation = 180f;
				break;

			case DirectionFacing.Left:
				rotation = -90f;
				break;
		}

		SetDirection(rotation);
	}

	public Vector2Int AttachToGrid()
	{
		gridPos = new Vector2Int(
			Mathf.RoundToInt(transform.position.x / Global.GRID_SIZE),
			Mathf.RoundToInt(transform.position.z / Global.GRID_SIZE)
			);
		GridController grid = GameObject.FindGameObjectWithTag("GridController").GetComponent<GridController>();
		grid.OccupyGridPos(this, gridPos);

		return gridPos;
	}

	public Vector2Int GridPos
	{
		get { return gridPos; }
		set { gridPos = value; }
	}

	public Transform FXSpawner()
	{
		return fxSpawnPoint;
	}

	//TURN
	public bool HasTakenTurn
	{
		get { return hasTakenTurn; }
		set { hasTakenTurn = value; }
	}

	public void StartTurn()
	{
		if (Stats.Team == GamePhase.Boss)
		{
			aiController.FindAction(this);
		}
		else
		{
			//if stunned
			if (Stats.IsStunned)
			{
				if (Stats.StunnedDuration <= 0)
				{
					//remove stunned
					Stats.IsStunned = false;
					SetStunnedIcon(false);

					//add energy back to unit
					Stats.Energy += 15;
					SpawnText("+ 15 Energy!", positiveColour);
					GameObject.FindGameObjectWithTag("UIController").GetComponent<UIController>().GetHighlightedUnitUI().UpdateStats();

					//get actions
					StartSelectActionPhase();
				}
				else
				{
					//show text
					SpawnText("BURNED OUT!", negativeColour);
					Stats.StunnedDuration -= 1;

					//wait and end turn
					StartCoroutine(WaitAndEndTurn());
				}
			}
			else
			{
				//get actions
				StartSelectActionPhase();
			}

		}
	}

	public IEnumerator WaitAndEndTurn()
	{
		yield return new WaitForSeconds(waitTime);

		EndTurn();
	}

	private void StartSelectActionPhase()
	{
		GameObject.FindGameObjectWithTag("UIController").GetComponent<UIController>().SetPlayerButtons(true, this);

		//Show Action Buttons
		inputController.InputMode = InputState.SelectAction;
	}

	public void StartMoveAction()
	{
		inputController.InputMode = InputState.Move;

		//TODO Move to button
		GameObject.FindGameObjectWithTag("GridController").GetComponent<GridController>().FindMoveNodes(this);
	}

	public void PerformAction(Node node)
	{
		GridController grid = GameObject.FindGameObjectWithTag("GridController").GetComponent<GridController>();

		CanAct = false;

		TurnUnitToFaceTarget(node);

		//perform animation
		GetComponentInChildren<Animator>().SetTrigger(FindAnimation(selectedAbility.ability.GetAnimation()));

		//ActionController
		abilityController.PerformAbility(this, grid.GetNodeFromGrid(GridPos), node, SelectedAbility);
	}

	public void TurnUnitToFaceTarget(Node node)
	{
		float newRotation = GetComponent<Mover>().FindDirection(this, node);
		SetDirection(newRotation);
	}

	public void EndAction()
	{
		Actions -= 1;

		if (SelectedAbility != null)
		{
			UsedAbility = SelectedAbility;
			SelectedAbility = null;
		}

		//check notoriety
		if (Stats.NotorietyMaxed)
		{
			EndTurn();
		}
		else
		{
			if (Stats.Team != GamePhase.Boss)
			{
				StartSelectActionPhase();
			}
			else
			{
				EndTurn();
			}
		}
	}

	public void EndTurn()
	{
		//remove previous node
		PreviousNode = null;
		HasTakenTurn = true;

		if (canAct)
		{
			Stats.Notoriety -= Mathf.Min(1, Stats.Notoriety);
		}

		//set no input
		inputController.InputMode = InputState.None;

		//reduce active cooldowns
		ReduceActiveCooldowns();

		//set cooldown
		StartUsedActionCooldown();

		if (Stats.Team != GamePhase.Boss && !Stats.NotorietyMaxed)
		{
			SetEndTurnIcon(true);
		}

		GameObject.FindGameObjectWithTag("UIController").GetComponent<UIController>().SetSelectedUnitStats(null);

		BattleController battleController = GameObject.FindGameObjectWithTag("BattleController").GetComponent<BattleController>();
		
		if (Stats.NotorietyMaxed)
		{
			//checked if units remain
			battleController.BeginRemoveUnit(this);
		}
		else
		{
			battleController.SelectedUnit = null;
			battleController.CheckIfCurrentPhaseComplete();
		}
	}

	//STATS
	public StatsController Stats
	{
		get { return statsController; }
		set { statsController = value; }
	}

	private void ReduceActiveCooldowns()
	{
		foreach(AbilityStats ability in Stats.Abilities)
		{
			if (ability.currentCooldown > 0)
			{
				ability.currentCooldown -= 1;
			}
		}
	}

	private void StartUsedActionCooldown()
	{
		if (UsedAbility != null)
		{
			UsedAbility.currentCooldown = usedAbility.ability.Cooldown();
			UsedAbility = null;
		}
	}

	public Unit PreviousUnit
	{
		get { return previousUnit; }
		set { previousUnit = value; }
	}

	public Group PreviousGroup
	{
		get { return previousGroup; }
		set { previousGroup = value; }
	}

	public void WipePreviousTargets()
	{
		PreviousUnit = null;
		PreviousGroup = null;
	}

	//TURN STATS
	public void SetActions()
	{
		HasTakenTurn = false;
		Actions = 2;
		CanMove = true;
		CanAct = true;
	}

	public int BossGroupIndex
	{
		get { return bossGroupIndex; }
		set { bossGroupIndex = value; }
	}

	public int Actions
	{
		get { return actions; }
		set { actions = value; }
	}

	public bool CanMove
	{
		get { return canMove; }
		set { canMove = value; }
	}

	public bool CanAct
	{
		get { return canAct; }
		set { canAct = value; }
	}

	public AbilityStats SelectedAbility
	{
		get { return selectedAbility; }
		set { selectedAbility = value; }
	}

	public AbilityStats UsedAbility
	{
		get { return usedAbility; }
		set { usedAbility = value; }
	}

	//MOVEMENT
	public void StartMovement(Node targetNode)
	{
		if (!isMoving)
		{
			GridController grid = GameObject.FindGameObjectWithTag("GridController").GetComponent<GridController>();

			//Get the path from the path controller
			List<Node> path = grid.GetPath(grid.GetNodeFromGrid(gridPos), targetNode);

			grid.WipeNodes();

			//Send the path to my mover to begin movement
			StartCoroutine(myMover.FollowPath(path, this));

			isMoving = true;
		}
	}

	public bool IsMoving
	{
		get { return isMoving; }
		set
		{
			isMoving = value;
		}
	}

	public void SetIsMoving(bool value)
	{
		isMoving = value;
	}

	public Node PreviousNode
	{
		get { return previousNode; }
		set { previousNode = value; }
	}

	//ANIMATION
	private string FindAnimation(AnimationType anim)
	{
		switch (anim)
		{
			case AnimationType.Talk1:

				return "Talk1";

			case AnimationType.Talk2:

				return "Talk2";

			case AnimationType.Talk3:

				return "Talk3";

			case AnimationType.Talk4:

				return "Talk4";

			case AnimationType.Talk5:

				return "Talk5";

			case AnimationType.Talk6:

				return "Talk6";

			case AnimationType.Talk7:

				return "Talk7";

			case AnimationType.Talk8:

				return "Talk8";

			default:
				return null;


		}

	}

	public DirectionFacing Direction
	{
		get { return directionFacing; }
		set { directionFacing = value; }
	}

	public void SetDirection(float facing)
	{
		transform.rotation = Quaternion.Euler(0, facing, 0);
	}

	//UI
	public void SpawnText(string value, Color colour)
	{
		GetComponent<TextSpawner>().SpawnText(value, colour);
	}

	public void SetEndTurnIcon(bool value)
	{
		endTurnIcon.SetActive(value);
	}

	public void SetStunnedIcon(bool value)
	{
		stunnedIcon.SetActive(value);
	}

	public void SetTalkingGraphic(bool value)
	{
		if (value)
		{
			talkingGraphic.SetActive(value);
			talkingGraphic.GetComponent<ParticleSystem>().Play();
		}
		else
		{
			talkingGraphic.GetComponent<ParticleSystem>().Stop();
			talkingGraphic.SetActive(value);
		}
	}
}
