﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsController : MonoBehaviour
{
	[SerializeField] UnitStats unitStats;

	string unitName = "error";
	GamePhase team;
	bool isCandidate;
	UnitStats chosenCandidate;
	Sprite unitPicture;
	Sprite unitHeadshot;
	int startingWit;
	int startingSelfEsteem;
	int startingPersuasion;
	int wit;
	int selfEsteem;
	int persuasion;
	float energy;
	[SerializeField] int notoriety = 0;
	float promotionChance = 0;
	int moveRange;
	bool isStunned;
	int stunnedDuration;
	bool notorietyMaxed;

	List<AbilityStats> abilities = new List<AbilityStats>();

	public void InitializeCharacter()
	{
		Name = unitStats.GetName();
		Team = unitStats.GetTeam();

		//create unit prefab
		GameObject prefab = Instantiate(unitStats.GetPrefab(), transform.position, Quaternion.identity, transform);
		prefab.GetComponent<Animator>().runtimeAnimatorController = unitStats.GetAnimator();

		Picture = unitStats.GetPicture();
		Headshot = unitStats.GetHeadshot();

		isCandidate = unitStats.GetIsCandidate();
		chosenCandidate = unitStats.GetChosenCandidate();
		                                  
		//Get starting stats
		startingWit = unitStats.GetStartingWit();
		startingSelfEsteem = unitStats.GetStartingSelfEsteem();
		startingPersuasion = unitStats.GetStartingPersuasion();
		promotionChance = unitStats.GetStartingPromotionChance();

		//set stat variables
		Wit = startingWit;
		SelfEsteem = startingSelfEsteem;
		Persuasion = startingPersuasion;

		Energy = unitStats.GetEnergy();

		foreach(Ability ability in unitStats.GetAbilities())
		{
			AbilityStats abilityStat = new AbilityStats();
			abilityStat.ability = ability;

			abilities.Add(abilityStat);
		}

		//get move range
		MoveRange = unitStats.GetMoveRange();
	}

	//STATS
	public UnitStats GetUnitStats()
	{
		return unitStats;
	}

	public string Name
	{
		get { return unitName; }
		set { unitName = value; }
	}

	public Sprite Picture
	{
		get { return unitPicture; }
		set { unitPicture = value; }
	}

	public Sprite Headshot
	{
		get { return unitHeadshot; }
		set { unitHeadshot = value; }
	}

	public int Wit
	{
		get { return wit; }
		set { wit = value; }
	}

	public int SelfEsteem
	{
		get { return selfEsteem; }
		set { selfEsteem = value; }
	}

	public int Persuasion
	{
		get { return persuasion; }
		set { persuasion = value; }
	}

	public float Energy
	{
		get { return energy; }
		set { energy = value; }
	}

	public int Notoriety
	{
		get { return notoriety; }
		set { notoriety = value; }
	}

	public float PromotionChance
	{
		get { return promotionChance; }
		set { promotionChance = value; }
	}

	public int MoveRange
	{
		get { return moveRange; }
		set { moveRange = value; }
	}

	public GamePhase Team
	{
		get { return team; }
		set { team = value; }
	}

	public bool Candidate
	{
		get { return isCandidate; }
		set { isCandidate = value; }
	}

	public UnitStats ChosenCandidate
	{
		get { return chosenCandidate; }
	}

	public List<AbilityStats> Abilities
	{
		get { return abilities; }
		set { abilities = value; }
	}

	public void ActivateCooldown(AbilityStats ability)
	{
		ability.currentCooldown = ability.ability.Cooldown();
	}

	public void ReduceCooldown(AbilityStats ability)
	{
		ability.currentCooldown -= 1;
	}

	public bool IsStunned
	{
		get { return isStunned; }
		set { isStunned = value; }
	}

	public int StunnedDuration
	{
		get { return stunnedDuration; }
		set { stunnedDuration = value; }
	}

	public bool NotorietyMaxed
	{
		get { return notorietyMaxed; }
		set { notorietyMaxed = value; }
	}

}

[System.Serializable]
public class AbilityStats
{
	public Ability ability;
	public int currentCooldown = 0;
}
