﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Group : MonoBehaviour
{
    [SerializeField] string groupName;
	[SerializeField] int wit = 5;
	//NPC's must always be at 0,0,0 rotation to start
	[SerializeField] GameObject NPC;
	[SerializeField] DirectionFacing NPCStartingDirection;
	[SerializeField] GameObject talkingGraphic;
	[SerializeField] Sprite profileSprite;

	public List<CandidateStats> candidateStats = new List<CandidateStats>();

	Vector2Int gridPos = new Vector2Int(0, 0);

	private void Start()
	{
		SetNPCRotation(NPCStartingDirection);
	}

	public DirectionFacing StartingRotation
	{
		get { return NPCStartingDirection; }
	}

	public void SetNPCRotation(DirectionFacing direction)
	{
		switch (direction)
		{
			case DirectionFacing.Up:
				NPC.transform.rotation = Quaternion.Euler(0, 0, 0);
				break;

			case DirectionFacing.Right:
				NPC.transform.rotation = Quaternion.Euler(0, 90, 0);
				break;

			case DirectionFacing.Down:
				NPC.transform.rotation = Quaternion.Euler(0, 180, 0);
				break;

			case DirectionFacing.Left:
				NPC.transform.rotation = Quaternion.Euler(0, -90, 0);
				break;
		}
	}

	public List<CandidateStats> GetStatsList()
	{
		return candidateStats;
	}

	public string GroupName
	{
		get { return groupName; }
		set { groupName = value; }
	}

	public Vector2Int AttachToGrid()
	{
		gridPos = new Vector2Int(
			Mathf.RoundToInt(transform.position.x / Global.GRID_SIZE),
			Mathf.RoundToInt(transform.position.z / Global.GRID_SIZE)
			);
		GridController grid = GameObject.FindGameObjectWithTag("GridController").GetComponent<GridController>();
		grid.OccupyGroupGridPos(this, gridPos);

		return gridPos;
	}

	public Vector2Int GridPos
	{
		get { return gridPos; }
		set { gridPos = value; }
	}

	public void InitializeCandidateStats(List<Unit> candidateList)
	{
		foreach(Unit candidate in candidateList)
		{
			//create candidate stats
			CandidateStats stats = new CandidateStats();
			stats.candidate = candidate;
			stats.modifier = 0;

			candidateStats.Add(stats);
		}
	}

	public void ChangeModifier(Unit candidate, bool isPositive, int amount)
	{
		//find unit
		foreach(CandidateStats stats in candidateStats)
		{
			if (stats.candidate == candidate)
			{
				//if it is positive
				if (isPositive)
				{
					//add to unit's modifier
					stats.modifier += amount;
				}
				else
				{
					//Remove from unit's modifier
					stats.modifier -= amount;
				}

				break;
			}
		}
	}

	//give modifiers to boss

	public void WipeModifiers()
	{
		foreach(CandidateStats candidate in candidateStats)
		{
			candidate.modifier = 0;
		}
	}

	public int Wit
	{
		get { return wit; }
		set { wit = value; }
	}

	public Sprite ProfileSprite()
	{
		return profileSprite;
	}

	public void SetNPCAnimation(bool value)
	{
		NPC.GetComponent<FindRandomAnimation>().CanAnimate = value;
	}

	public DirectionFacing FindDirection(Unit unit)
	{
		DirectionFacing directionFacing = NPCStartingDirection;

		Vector2Int targetGridPos = unit.GridPos;

		//right +1, 0
		if (GridPos.x < targetGridPos.x && GridPos.y == targetGridPos.y)
		{
			directionFacing = DirectionFacing.Right;
		}
		//up 0, -1
		else if (gridPos.x == targetGridPos.x && gridPos.y < targetGridPos.y)
		{
			directionFacing = DirectionFacing.Up;
		}
		//left -1, 0
		else if (gridPos.x > targetGridPos.x && gridPos.y == targetGridPos.y)
		{
			directionFacing = DirectionFacing.Left;
		}
		//down 0, +1
		else if (gridPos.x == targetGridPos.x && gridPos.y > targetGridPos.y)
		{
			directionFacing = DirectionFacing.Down;
		}

		return directionFacing;
		
	}

	public void SetTalkingGraphic(bool value)
	{
		if (value)
		{
			talkingGraphic.SetActive(value);
			talkingGraphic.GetComponent<ParticleSystem>().Play();
		}
		else
		{
			talkingGraphic.GetComponent<ParticleSystem>().Stop();
			talkingGraphic.SetActive(value);
		}
		
	}
}

[System.Serializable]
public class CandidateStats
{
	public Unit candidate;
	public int modifier;
}
