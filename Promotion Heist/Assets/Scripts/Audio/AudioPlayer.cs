﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
    //Components
    AudioSource audioSource;

    //State Variables
    AudioClip audioClip;
    [SerializeField] float targetVolume = .4f;
    float fadeTime = 1.5f;
    bool volumeFadedIn = false;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void SetTargetVolume(float targetVolume)
    {
        this.targetVolume = targetVolume;
    }

    public void SetVolume(float newVolume)
    {
        targetVolume = newVolume;

        if (volumeFadedIn)
        {
            audioSource.volume = targetVolume;
        }
    }

    public void SetNextClip(AudioClip newClip)
    {
        audioClip = newClip;
    }

    public void InitializeMusic()
    {
        audioSource.clip = audioClip;
        StartCoroutine(FadeInAudio(fadeTime));
    }

    public IEnumerator FadeOutAudio(float fadeTime)
    {
        //stop volume from being adjusted in Volume Settings
        volumeFadedIn = false;

        //Stop audio from fading in if it is already
        StopCoroutine(FadeInAudio(fadeTime));

        //Set starting value
        float startVolume = audioSource.volume;

        //Reduce volume to 0
        while (audioSource.volume > 0)
        {
            audioSource.volume -= startVolume * (Time.deltaTime / fadeTime);

            yield return null;
        }
    }

    public IEnumerator FadeOutAndInAudio(float fadeTime)
    {
        //Stop other coroutines if they are running
        StopCoroutine(FadeInAudio(fadeTime));

        //Set starting value
        float startVolume = audioSource.volume;

        //Reduce volume to 0
        while (audioSource.volume > 0)
        {
            audioSource.volume -= startVolume * (Time.deltaTime / fadeTime);

            yield return null;
        }

        //set and play the next song in the audiosource
        audioSource.clip = audioClip;
        audioSource.Play();

        //Start fading in the audio
        StartCoroutine(FadeInAudio(fadeTime));
    }

    public IEnumerator FadeInAudio(float fadeTime)
    {
        //Stop other coroutines if they are running
        StopCoroutine(FadeOutAudio(fadeTime));
        StopCoroutine(FadeOutAndInAudio(fadeTime));

        //Play audio
        audioSource.Play();

        //fade in audio to specified volume
        while (audioSource.volume < targetVolume)
        {
            audioSource.volume += targetVolume * (Time.deltaTime / fadeTime);

            yield return null;
        }
        
        //allow volume to be adjusted in Volume Settings
        volumeFadedIn = true;
    }
}
