﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DirectionFacing
{
    Right,
    Up,
    Left,
    Down
}

public class Mover : MonoBehaviour
{
    [SerializeField] float movementTolerance;
    [SerializeField] float moveSpeed = 1f;
    [Range(0,1)]
    [SerializeField] float animSpeed = .5f;

    IEnumerator movementCoroutine;

    float storedMoveSpeed = 0f;
    bool isMoving = false;

    private void Start()
    {
        storedMoveSpeed = moveSpeed;
    }

	private void Update()
	{
		if (isMoving)
		{
            //set movement speed
            GetComponentInChildren<Animator>().SetFloat("velocity", animSpeed);
        }
	}

	public void SetIsPaused(bool value)
    {
        if (value)
        {
            moveSpeed = 0f;
        }
        else
        {
            moveSpeed = storedMoveSpeed;
        }
    }

    public IEnumerator FollowPath(List<Node> path, Unit unit)
    {
        GridController gridController = GameObject.FindGameObjectWithTag("GridController").GetComponent<GridController>();
        unit.SetIsMoving(true);
        isMoving = true;
        gridController.ClearNodeColour();
        gridController.EmptyGridPos(unit.GridPos);

        //go through waypoints in order
        foreach (Node node in path.ToArray())
        {
            movementCoroutine = MoveToNode(node, unit);

            yield return movementCoroutine;

            //set unit's grid position 
            unit.GridPos = node.GridPos;
        }

        //stop movement speed
        isMoving = false;
        GetComponentInChildren<Animator>().SetFloat("velocity", 0);

        //wipe grid 
        gridController.WipePath();
        gridController.OccupyGridPos(unit, unit.GridPos);
        gridController.ColourNodes();

        //end unit's turn
        unit.SetIsMoving(false);
        unit.CanMove = false;
        unit.EndAction();
    }

    public IEnumerator AIFollowPath(List<Node> path, Unit unit)
    {
        GridController gridController = GameObject.FindGameObjectWithTag("GridController").GetComponent<GridController>();
        unit.SetIsMoving(true);
        isMoving = true;
        gridController.EmptyGridPos(unit.GridPos);
        int index = 0;

        //if boss unit
        if (unit.Stats.Team == GamePhase.Boss)
		{
            gridController.WipeBossRangeNodes();
		}
        gridController.ClearNodeColour();

        //go through waypoints in order
        foreach (Node node in path.ToArray())
        {
            movementCoroutine = MoveToNode(node, unit);

            yield return movementCoroutine;

            //set unit's grid position 
            unit.GridPos = node.GridPos;
        }


        //stop movement speed
        isMoving = false;
        GetComponentInChildren<Animator>().SetFloat("velocity", 0);

        //if boss unit
        if (unit.Stats.Team == GamePhase.Boss)
        {
            gridController.FindBossRangeNodes(unit);
        }

        //wipe grid 
        gridController.WipeNodes();
        gridController.WipePath();
        gridController.OccupyGridPos(unit, unit.GridPos);

        gridController.ColourNodes();

        //end unit's turn
        unit.SetIsMoving(false);
        unit.CanMove = false;
    }

    private IEnumerator MoveToNode(Node targetNode, Unit unit)
    {
        unit.SetDirection(FindDirection(unit, targetNode));

        //if the player isnt where it should be
        while (Vector3.Distance(transform.position, targetNode.transform.position) > movementTolerance)
        {
            //move towards the position
            transform.position = Vector3.MoveTowards(transform.position, targetNode.transform.position, moveSpeed * Time.deltaTime);

            yield return null;
        }
    }

    public float FindDirection(Unit unit, Node targetNode)
    {
        float rotation = 0f;

        Vector2Int gridPos = unit.GridPos;
        Vector2Int targetGridPos = targetNode.GridPos;

        //right +1, 0
        if (gridPos.x < targetGridPos.x && gridPos.y == targetGridPos.y)
        {
            rotation = 90f;
            unit.Direction = DirectionFacing.Right;
        }
        //up 0, -1
        else if (gridPos.x == targetGridPos.x && gridPos.y < targetGridPos.y)
        {
            rotation = 0;
            unit.Direction = DirectionFacing.Up;
        }
        //left -1, 0
        else if (gridPos.x > targetGridPos.x && gridPos.y == targetGridPos.y)
        {
            rotation = -90f;
            unit.Direction = DirectionFacing.Left;
        }
        //down 0, +1
        else if (gridPos.x == targetGridPos.x && gridPos.y > targetGridPos.y)
        {
            rotation = 180f;
            unit.Direction = DirectionFacing.Down;
        }

        return rotation;
    }


}
