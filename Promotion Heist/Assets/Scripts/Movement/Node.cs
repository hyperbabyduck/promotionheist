﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Node : MonoBehaviour, IRaycastable
{
    [Header("Node UI")]
    [SerializeField] SpriteRenderer nodeOverlay;
    [SerializeField] SpriteRenderer bossRangeIcon;
    [SerializeField] float movementCost = 1f;
    

    //Colours
    Color moveNodeColour;
    Color selectedMoveNodeColour;
    Color rangeNodeColour;
    Color selectedRangeNodeColour;
    Color blueTeamColour;
    Color blueTeamHighlightedColour;
    Color redTeamColour;
    Color redTeamHighlightedColour;
    Color yellowTeamColour;
    Color yellowTeamHighlightedColour;
    Color bossOccupiedNodeColour;
    Color selectedBossOccupiedNodeColour;
    Color groupNodeColour;
    Color selectedGroupNodeColour;
    Color bossRangeNodeColour;

    Vector2Int gridPos = new Vector2Int(0, 0);

    //Node Stats
    bool isExplored = false;
    public Node exploredFrom = default;
    bool isOccupied = false;
    Unit occupant = null;
    Group groupOccupant;
    public bool isMoveNode = false;
    bool isRangeNode = false;
    bool isGroupNode = false;
    bool isBossRangeNode = false;

    float totalMovementCost = 0f;

    bool isHighlighted = false;

    //PATHFINDING
    public Vector2Int AttachToGrid()
    {
        gridPos = new Vector2Int(
            Mathf.RoundToInt(transform.position.x / Global.GRID_SIZE),
            Mathf.RoundToInt(transform.position.z / Global.GRID_SIZE)
            );

        return gridPos;
    }

    public Vector2Int GridPos
    {
        get { return gridPos; }
        set { gridPos = value; }
    }

    public bool IsExplored
    {
        get{ return isExplored; }
        set{ isExplored = value; }
    }

    public Node ExploredFrom
    {
        get{ return exploredFrom; }
        set{ exploredFrom = value; }
    }

    //NODE INFO
    public bool IsOccupied
    {
        get{ return isOccupied; }
        set{ isOccupied = value; }
    }

    public Unit Occupant
    {
        get{ return occupant; }
        set{ occupant = value; }
    }

    public Group GroupOccupant
	{
		get { return groupOccupant; }
		set { groupOccupant = value; }
	}

    public float TotalMovementCost
    {
        get { return totalMovementCost; }
        set { totalMovementCost = value; }
    }

    public float MovementCost
    {
        get { return movementCost; }
        set { movementCost = value; }
    }

    public bool IsMoveNode
    {
        get { return isMoveNode; }
        set { isMoveNode = value; }
    }

    public bool IsRangeNode
    {
        get { return isRangeNode; }
        set { isRangeNode = value; }
    }

    public bool IsGroupNode
	{
		get { return isGroupNode; }
        set { isGroupNode = true; }
	}

    public bool IsBossRangeNode
	{
        get 
        { return isBossRangeNode; }
        set 
        {
            bossRangeIcon.enabled = value;
            isBossRangeNode = value; 
        }
	}

    public void SetColourValues(NodeColours nodeColours)
    {
        moveNodeColour = nodeColours.moveNodeColour;
        selectedMoveNodeColour = nodeColours.selectedMoveNodeColour;
        rangeNodeColour = nodeColours.rangeNodeColour;
        selectedRangeNodeColour = nodeColours.selectedRangeNodeColour;
        blueTeamColour = nodeColours.blueTeamColour;
        blueTeamHighlightedColour = nodeColours.blueHighlightedTeamColour;
        redTeamColour = nodeColours.redTeamColour;
        redTeamHighlightedColour = nodeColours.redHighlightedTeamColour;
        yellowTeamColour = nodeColours.yellowTeamColour;
        yellowTeamHighlightedColour = nodeColours.yellowHighlightedTeamColour;
        bossOccupiedNodeColour = nodeColours.bossOccupiedNodeColour;
        selectedBossOccupiedNodeColour = nodeColours.selectedBossOccupiedNodeColour;
        groupNodeColour = nodeColours.groupNodeColour;
        selectedGroupNodeColour = nodeColours.selectedGroupNodeColour;
        bossRangeNodeColour = nodeColours.bossRangeNodeColour;
    }

    //UI
    public bool Highlighted
    {
        get { return isHighlighted; }
        set { isHighlighted = value; }
    }

    public void ColourNode()
    {
        //if move node
        if (IsMoveNode)
        {
            //if highlighted
            if (Highlighted)
			{
                nodeOverlay.color = selectedMoveNodeColour;
			}
            else
            {
                nodeOverlay.color = moveNodeColour;
            }

            //show node
            nodeOverlay.enabled = true;
        }
        else if (IsRangeNode)
        {
            if (IsOccupied)
            {
                //if a group node
                if (IsGroupNode)
				{
                    if (Highlighted)
					{
                        nodeOverlay.color = selectedRangeNodeColour; ;
                    }
					else
					{
                        nodeOverlay.color = groupNodeColour;
					}
				}
				else
				//if occupied by a unit
				{
					SetOccupantColour();
				}

			}
            else //not occupied
            {
                if (IsGroupNode)
                {
                    if (Highlighted)
                    {
                        nodeOverlay.color = selectedGroupNodeColour;
                    }
                    else
                    {
                        nodeOverlay.color = groupNodeColour;
                    }
                }
				else
				{
                    //if highlighted
                    if (Highlighted)
                    {
                        nodeOverlay.color = selectedRangeNodeColour;
                    }
                    else
                    {
                        nodeOverlay.color = rangeNodeColour;
                    }
                }
            }

            //show overlay
            nodeOverlay.enabled = true;
        }
        else
        {
            if (IsOccupied)
            {
                if (IsGroupNode)
                {
                    if (Highlighted)
                    {
                        nodeOverlay.color = selectedGroupNodeColour; ;
                    }
                    else
                    {
                        nodeOverlay.color = groupNodeColour;
                    }
                }
				else
				{
                    SetOccupantColour();
                }

                nodeOverlay.enabled = true;
            }
            else
            {
                if (Highlighted)
				{
                    nodeOverlay.enabled = true;
                    nodeOverlay.color = Color.white;
                }
                else
				{
                    if (isBossRangeNode)
					{
                        nodeOverlay.enabled = true;
                        nodeOverlay.color = bossRangeNodeColour;
                    }
					else
					{
                        nodeOverlay.enabled = false;
                        nodeOverlay.color = Color.white;
                    }
                }
                
            }
        }
    }

	private void SetOccupantColour()
	{
		switch (Occupant.Stats.Team)
		{
			case GamePhase.BlueTeam:
				//if highlighted
				if (Highlighted)
				{
					nodeOverlay.color = blueTeamHighlightedColour;
				}
				else
				{
					nodeOverlay.color = blueTeamColour;
				}
				break;

			case GamePhase.RedTeam:
				//if highlighted
				if (Highlighted)
				{
					nodeOverlay.color = redTeamHighlightedColour;
				}
				else
				{
					nodeOverlay.color = redTeamColour;
				}
				break;


			case GamePhase.YellowTeam:
				//if highlighted
				if (Highlighted)
				{
					nodeOverlay.color = yellowTeamHighlightedColour;
				}
				else
				{
					nodeOverlay.color = yellowTeamColour;
				}
				break;

			case GamePhase.Boss:
				//if highlighted
				if (Highlighted)
				{
					nodeOverlay.color = selectedRangeNodeColour;
				}
				else
				{
					nodeOverlay.color = bossOccupiedNodeColour;
				}
				break;

		}
	}

	public void SetNodeColour(Color colour, bool isVisible)
    {
        nodeOverlay.color = colour;
        nodeOverlay.enabled = isVisible;
    }

	public bool HandleRaycast()
	{
        return true;
	}
}
