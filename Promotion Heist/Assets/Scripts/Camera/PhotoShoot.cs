﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class PhotoShoot : MonoBehaviour
{
    [SerializeField] GameObject[] characterModels;
    [SerializeField] Transform spawnTransform;
    [SerializeField] Camera mainCamera;
    private int fileCounter;
    public int index = 0;

    GameObject currentModel;

    // Start is called before the first frame update
    void Start()
    {
        //ChangeModel();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
		{
            ChangeModel();
		}
    }

    private void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            TakePhoto();
        }
    }

    private void ChangeModel()
	{
        //delete
        if (currentModel != null)
		{
            Destroy(currentModel);
		}

        //instantiate
        currentModel = Instantiate(characterModels[index], spawnTransform.position, Quaternion.identity);
        currentModel.transform.parent = spawnTransform;

        //get from array
        if(index + 1 < characterModels.Length)
		{
            index += 1;
        }
        else
		{
            index = 0;
        }
    }

    private void TakePhoto()
    {
        ScreenCapture.CaptureScreenshot("Screenshot_" + index + ".png");
        fileCounter += 1;
    }
}
