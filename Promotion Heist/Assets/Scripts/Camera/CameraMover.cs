﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
    [SerializeField] InputController input;
	[SerializeField] BattleController battleController;
	[SerializeField] float moveSpeed = 1.5f;

	public float MoveSpeed()
	{
		return moveSpeed;
	}

	private void LateUpdate()
	{
		if (battleController.SelectedUnit != null)
		{
			if (battleController.SelectedUnit.IsMoving)
			{
				transform.position = battleController.SelectedUnit.transform.position;
			}
		}
	}

	public void MoveCamera(Vector3 target)
	{
		transform.position = target;
	}
}
