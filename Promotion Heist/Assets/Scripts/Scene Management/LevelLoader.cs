﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    [SerializeField] string mainMenu;
    [SerializeField] string level;
    [SerializeField] float loadTime = 1.5f;
    [SerializeField] float fadeInTime = 3f;
    [SerializeField] float fadeOutTime = 3f;
    [SerializeField] float UIFadeOutTime = 2f;

    string sceneToLoad;
    Fader fader;

    private void Start()
    {
        DontDestroyOnLoad(this);

        if (FindObjectsOfType<LevelLoader>().Length > 1)
        {
            Destroy(gameObject);
        }

        fader = FindObjectOfType<Fader>();
    }

    public void LoadLevel()
    {
        sceneToLoad = level;
        StartCoroutine(Transition());
    }

    public void RestartLevel()
    {
        LoadLevel();
    }

    public void LoadMainMenu()
    {
        sceneToLoad = mainMenu;
        StartCoroutine(TransitionToMenu());
    }

    public void CloseApplication()
    {
        Application.Quit();
    }

    private IEnumerator Transition()
    {
        //fade out UI
        UIFader UIfader = FindObjectOfType<UIFader>();
        UIfader.FadeOut(UIFadeOutTime);

        //Fade out Sound
        FadeOutSound(fadeOutTime);

        yield return fader.FadeOut(fadeOutTime);
        SceneManager.LoadScene(sceneToLoad);

        yield return new WaitForSeconds(loadTime);
        //Fade in sound
        //FadeInSound();

        //fade in screen
        yield return fader.FadeIn(fadeInTime);

        GameObject.FindGameObjectWithTag("BattleController").GetComponent<BattleController>().StartOpeningSceneDialogue();
    }

    private IEnumerator TransitionToMenu()
    {
        //Fade Out UI
        UIFader UIfader = FindObjectOfType<UIFader>();
        UIfader.FadeOut(UIFadeOutTime);

        //Fade out UI
        FadeOutSound(fadeOutTime);

        yield return fader.FadeOut(fadeOutTime);
        SceneManager.LoadScene(sceneToLoad);

        yield return new WaitForSeconds(loadTime);
        UIFader newUIfader = FindObjectOfType<UIFader>();

        fader.FadeIn(fadeInTime);

        newUIfader.FadeIn(fadeInTime);
    }

    private void FadeInSound()
    {
        AudioPlayer[] audioPlayers = FindObjectsOfType<AudioPlayer>();

        foreach(AudioPlayer audioPlayer in audioPlayers)
		{
            audioPlayer.InitializeMusic();
        }
        
    }

    private void FadeOutSound(float fadeOutTime)
    {
        AudioPlayer[] audioPlayers = FindObjectsOfType<AudioPlayer>();

        foreach (AudioPlayer audioPlayer in audioPlayers)
        {
            StartCoroutine(audioPlayer.FadeOutAudio(fadeOutTime));
        }
    }
}
