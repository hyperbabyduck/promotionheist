﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fader : MonoBehaviour
{
    [SerializeField] float initialFadeInTime = 2f;
    CanvasGroup canvasGroup;
    Coroutine currentActiveFade = null;

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    private void Start()
    {
        DontDestroyOnLoad(this);

        if (FindObjectsOfType<Fader>().Length > 1)
        {
            Destroy(gameObject);
            return;
        }

        StartCoroutine(FadeRoutine(0, initialFadeInTime));
        FadeInUI();
    }

    private void FadeInUI()
    {
        UIFader UIfader = FindObjectOfType<UIFader>();
        UIfader.FadeIn(initialFadeInTime);
    }

    public void FadeOutImmediate()
    {
        canvasGroup.alpha = 1;
        canvasGroup.blocksRaycasts = true;
    }

    public Coroutine FadeOut(float time)
    {
        return Fade(1, time);
    }

    public Coroutine FadeIn(float time)
    {
        return Fade(0, time);
    }

    public Coroutine Fade(float target, float time)
    {
        currentActiveFade = StartCoroutine(FadeRoutine(target, time));
        return currentActiveFade;
    }

    private IEnumerator FadeRoutine(float target, float time)
    {
        while (!Mathf.Approximately(canvasGroup.alpha, target))
        {
            canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, target, Time.deltaTime / time);
            if (canvasGroup.alpha < 0)
			{
                canvasGroup.blocksRaycasts = true;
            }
			else
			{
                canvasGroup.blocksRaycasts = false;
            }
            yield return null;
        }
    }
}
