﻿using PixelCrushers.DialogueSystem.UnityGUI;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using UnityEngine;

public enum PlayerStat
{
	None,
	Wit,
	SelfEsteem,
	Persuasion
}

public enum AnimationType
{
	Talk1,
	Talk2,
	Talk3,
	Talk4,
	Talk5,
	Talk6,
	Talk7,
	Talk8,
}

public class AbilityController : MonoBehaviour
{
	[SerializeField] int bossMultiplier = 2;
	[SerializeField] UIController uiController;
	[SerializeField] GridController grid;
	[SerializeField] int setNumber = 30;
	[SerializeField] int targetNumber = 10;
	[Header("Effects")]
	[SerializeField] GameObject energyUpEffect;
	[SerializeField] GameObject energyDownEffect;
	[SerializeField] GameObject stunnedEffect;

	Unit target;
	Group targetGroup;
	Unit candidate;


	public void PerformAbility(Unit unit, Node unitNode, Node targetNode, AbilityStats ability)
	{

		//start the unit's talking effect
		unit.SetTalkingGraphic(true);

		switch (ability.ability.GetName())
		{
			case "Talk Up":
				#region
				//can only happen to unit
				//get target unit

				candidate = null;

				//find the candidate this unit represents
				if (unit.Stats.Candidate)
				{
					candidate = unit;
				}
				else
				{
					Unit[] units = FindObjectsOfType<Unit>();
					foreach (Unit possibleCandidate in units)
					{
						if (possibleCandidate.Stats.GetUnitStats() == unit.Stats.ChosenCandidate)
						{
							candidate = possibleCandidate;
							break;
						}
					}
				}

				//if target is group
				if (targetNode.IsGroupNode)
				{
					targetGroup = targetNode.GroupOccupant;
					//start the unit's talking effect
					targetGroup.SetTalkingGraphic(true);
					targetGroup.SetNPCRotation(targetGroup.FindDirection(unit));
					targetGroup.SetNPCAnimation(false);

					//check if action succeeds
					if (CheckSuccess(targetGroup.Wit, ability.ability.Difficulty))
					{
						uiController.AddToTextList("SUCCESS", true, false, unit, null, false, null, false, null, TextColour.Positive, ability.ability.SuccessEffect(), unit.FXSpawner());

						//increase groups standing
						targetGroup.ChangeModifier(candidate, true, unit.Stats.Persuasion);
						uiController.AddToTextList(candidate.Stats.Name + " " + unit.Stats.Persuasion, false, false, null, null, false, null, false, uiController.GetGroupUI(), TextColour.Positive, null, null);

						//if node is in boss's range
						if (targetNode.IsBossRangeNode || unitNode.IsBossRangeNode)
						{
							//increase notoriety
							unit.Stats.Notoriety += Mathf.Min(ability.ability.SuccessNotoriety(), 10 - unit.Stats.Notoriety);
							CheckNotoriety(unit);
							uiController.AddToTextList(ability.ability.SuccessNotoriety().ToString() + " NOTORIETY", false, false, null, null, true, uiController.GetLeftUnitUI(), false, null, TextColour.Negative, null, null);
						}
					}
					else
					{
						uiController.AddToTextList("FAIL", true, false, unit, null, false, null, false, null, TextColour.Negative, ability.ability.FailureEffect(), unit.FXSpawner());

						//decrease groups standing
						targetGroup.ChangeModifier(candidate, false, unit.Stats.Persuasion);
						uiController.AddToTextList(candidate.Stats.Name + " -" + unit.Stats.Persuasion, false, false, null, null, false, null, false, uiController.GetGroupUI(), TextColour.Negative, null, null);

						//if node is in boss's range
						if (targetNode.IsBossRangeNode || unitNode.IsBossRangeNode)
						{
							//increase notoriety
							unit.Stats.Notoriety += Mathf.Min(ability.ability.FailureNotoriety(), 10 - unit.Stats.Notoriety);
							CheckNotoriety(unit);
							uiController.AddToTextList(ability.ability.FailureNotoriety().ToString() + " NOTORIETY", false, false, null, null, true, uiController.GetLeftUnitUI(), false, null, TextColour.Negative, null, null);
						}
					}

				}
				//if target is boss
				else if (targetNode.IsOccupied && targetNode.Occupant.Stats.Team == GamePhase.Boss)
				{
					target = targetNode.Occupant;
					target.SetTalkingGraphic(true);

					//turn target to face unit
					target.TurnUnitToFaceTarget(unitNode);

					//check if action succeeds
					if (CheckSuccess(target.Stats.Wit, ability.ability.Difficulty))
					{
						uiController.AddToTextList("SUCCESS", true, false, unit, null, false, null, false, null, TextColour.Positive, ability.ability.SuccessEffect(), unit.FXSpawner());

						//increase notoriety more
						unit.Stats.Notoriety += ability.ability.SuccessNotoriety();
						uiController.AddToTextList(ability.ability.SuccessNotoriety().ToString() + " NOTORIETY", false, false, null, null, true, uiController.GetLeftUnitUI(), false, null, TextColour.Negative, null, null);

						//decrease candidates's boss standing directly
						candidate.Stats.PromotionChance += Mathf.Min(unit.Stats.Persuasion * bossMultiplier, candidate.Stats.PromotionChance);
						uiController.AddToTextList(candidate.Stats.Name + " " + unit.Stats.Persuasion * bossMultiplier, false, false, null, null, false, uiController.GetRightUnitUI(), false, null, TextColour.Negative, null, null);
					}
					else
					{
						uiController.AddToTextList("FAIL", true, false, unit, null, false, null, false, null, TextColour.Negative, ability.ability.FailureEffect(), unit.FXSpawner());

						//increase notoriety more
						unit.Stats.Notoriety += ability.ability.FailureNotoriety();
						uiController.AddToTextList(ability.ability.FailureNotoriety().ToString() + " NOTORIETY", false, false, null, null, true, uiController.GetLeftUnitUI(), false, null, TextColour.Negative, null, null);

						//decrease candidates's boss standing directly
						candidate.Stats.PromotionChance -= Mathf.Min(unit.Stats.Persuasion * bossMultiplier, candidate.Stats.PromotionChance);
						uiController.AddToTextList(candidate.Stats.Name + " -" + unit.Stats.Persuasion * bossMultiplier, false, false, null, null, false, uiController.GetRightUnitUI(), false, null, TextColour.Negative, null, null);
					}
				}
				//wipe nodes
				grid.WipeNodes();

				//show UI Text
				StartCoroutine(uiController.ShowDamageText(unit, targetNode.Occupant, targetNode.GroupOccupant));
				#endregion
				break;

			case "Bad Mouth":
				#region
				candidate = null;

				//find the candidate this unit represents
				if (unit.Stats.Candidate)
				{
					candidate = unit;
				}
				else
				{
					Unit[] units = FindObjectsOfType<Unit>();
					foreach (Unit possibleCandidate in units)
					{
						if (possibleCandidate.Stats.GetUnitStats() == unit.Stats.ChosenCandidate)
						{
							candidate = possibleCandidate;
							break;
						}
					}
				}

				//if target is group
				if (targetNode.IsGroupNode)
				{
					targetGroup = targetNode.GroupOccupant;
					targetGroup.SetTalkingGraphic(true);
					targetGroup.SetNPCRotation(targetGroup.FindDirection(unit));
					targetGroup.SetNPCAnimation(false);

					//check if action succeeds
					if (CheckSuccess(targetGroup.Wit, ability.ability.Difficulty))
					{
						uiController.AddToTextList("SUCCESS", true, false, unit, null, false, null, false, null, TextColour.Positive, ability.ability.SuccessEffect(), unit.FXSpawner());

						//decrease groups standing for other candidates
						foreach (Unit possibleCandidate in GetComponent<UnitController>().GetList(GamePhase.Candidate))
						{
							//if not the candidate the current unit is representing
							if (candidate != possibleCandidate)
							{
								targetGroup.ChangeModifier(possibleCandidate, false, unit.Stats.Persuasion);
								uiController.AddToTextList(possibleCandidate.Stats.Name + " -" + unit.Stats.Persuasion, false, false, null, null, false, null, false, uiController.GetGroupUI(), TextColour.Negative, null, null);
							}
						}

						//if node is in boss's range
						if (targetNode.IsBossRangeNode || unitNode.IsBossRangeNode)
						{
							//increase notoriety
							unit.Stats.Notoriety += Mathf.Min(ability.ability.SuccessNotoriety(), 10 - unit.Stats.Notoriety);
							CheckNotoriety(unit);
							uiController.AddToTextList(ability.ability.SuccessNotoriety().ToString() + " NOTORIETY", false, false, null, null, true, uiController.GetLeftUnitUI(), false, null, TextColour.Negative, null, null);
						}
					}
					else
					{
						uiController.AddToTextList("FAIL", true, false, unit, null, false, null, false, null, TextColour.Negative, ability.ability.FailureEffect(), unit.FXSpawner());

						//decrease groups standing
						targetGroup.ChangeModifier(candidate, false, unit.Stats.Persuasion);
						uiController.AddToTextList(candidate.Stats.Name + " -" + unit.Stats.Persuasion, false, false, null, null, false, null, false, uiController.GetGroupUI(), TextColour.Negative, null, null);

						//if node is in boss's range
						if (targetNode.IsBossRangeNode || unitNode.IsBossRangeNode)
						{
							//increase notoriety
							unit.Stats.Notoriety += Mathf.Min(ability.ability.FailureNotoriety(), 10 - unit.Stats.Notoriety);
							CheckNotoriety(unit);
							uiController.AddToTextList(ability.ability.FailureNotoriety().ToString() + " NOTORIETY", false, false, null, null, true, uiController.GetLeftUnitUI(), false, null, TextColour.Negative, null, null);
						}
					}
				}
				//if target is boss
				else if (targetNode.IsOccupied && targetNode.Occupant.Stats.Team == GamePhase.Boss)
				{
					target = targetNode.Occupant;
					target.SetTalkingGraphic(true);

					//turn target to face unit
					target.TurnUnitToFaceTarget(unitNode);

					//check if action succeeds
					if (CheckSuccess(target.Stats.Wit, ability.ability.Difficulty))
					{
						uiController.AddToTextList("SUCCESS", true, false, unit, null, false, null, false, null, TextColour.Positive, ability.ability.SuccessEffect(), target.FXSpawner());

						//decrease groups standing for other candidates
						foreach (Unit possibleCandidate in GetComponent<UnitController>().GetList(GamePhase.Candidate))
						{
							//if not the candidate the current unit is representing
							if (candidate != possibleCandidate)
							{
								//decrease candidates's boss standing directly
								possibleCandidate.Stats.PromotionChance -= Mathf.Min(unit.Stats.Persuasion * bossMultiplier, possibleCandidate.Stats.PromotionChance);

								uiController.AddToTextList(possibleCandidate.Stats.Name + " -" + unit.Stats.Persuasion, false, false, null, null, false, uiController.GetRightUnitUI(), false, null, TextColour.Negative, null, null);
							}
						}

						//increase notoriety
						unit.Stats.Notoriety += Mathf.Min(ability.ability.SuccessNotoriety(), 10 - unit.Stats.Notoriety);
						CheckNotoriety(unit);
						uiController.AddToTextList((ability.ability.SuccessNotoriety() * bossMultiplier).ToString() + " NOTORIETY", false, false, null, null, true, uiController.GetLeftUnitUI(), false, null, TextColour.Negative, null, null);

					}
					else
					{
						uiController.AddToTextList("FAIL", true, false, unit, null, false, null, false, null, TextColour.Negative, ability.ability.FailureEffect(), unit.FXSpawner());

						//decrease candidates's boss standing directly
						candidate.Stats.PromotionChance -= Mathf.Min(unit.Stats.Persuasion * bossMultiplier, candidate.Stats.PromotionChance);
						uiController.AddToTextList(candidate.Stats.Name + " -" + (unit.Stats.Persuasion * bossMultiplier).ToString(), false, false, null, null, false, uiController.GetRightUnitUI(), false, null, TextColour.Negative, null, null);

						//increase notoriety more
						unit.Stats.Notoriety += Mathf.Min(ability.ability.FailureNotoriety(), 10 - unit.Stats.Notoriety);
						CheckNotoriety(unit);
						uiController.AddToTextList((ability.ability.FailureNotoriety() * bossMultiplier).ToString() + " NOTORIETY", false, false, null, null, true, uiController.GetLeftUnitUI(), false, null, TextColour.Negative, null, null);
					}
				}

				//wipe nodes
				grid.WipeNodes();

				//show UI Text
				StartCoroutine(uiController.ShowDamageText(unit, targetNode.Occupant, targetNode.GroupOccupant));
				#endregion
				break;

			case "Demoralize":
				#region
				//can only happen to unit
				//get target unit
				target = targetNode.Occupant;
				target.SetTalkingGraphic(true);
				candidate = null; ;

				//find the candidate this unit represents
				if (unit.Stats.Candidate)
				{
					candidate = unit;
				}
				else
				{
					Unit[] units = FindObjectsOfType<Unit>();
					foreach (Unit possibleCandidate in units)
					{
						if (possibleCandidate.Stats.GetUnitStats() == unit.Stats.ChosenCandidate)
						{
							candidate = possibleCandidate;
							break;
						}
					}
				}

				//turn target to face unit
				target.TurnUnitToFaceTarget(unitNode);

				//if successful
				if (CheckSuccess(target.Stats.Wit, ability.ability.Difficulty))
				{
					uiController.AddToTextList("SUCCESS", true, false, unit, null, false, null, false, null, TextColour.Positive, ability.ability.SuccessEffect(), unit.FXSpawner());

					//reduce target energy by amount
					target.Stats.Energy -= Mathf.Min(EnergyReductionAmount(ability.ability.GetAmount(), target.Stats.SelfEsteem, unit.Stats.Persuasion, target.Stats.Energy), target.Stats.Energy);
					uiController.AddToTextList("-" + EnergyReductionAmount(ability.ability.GetAmount(), target.Stats.SelfEsteem, unit.Stats.Persuasion, target.Stats.Energy).ToString() + " ENERGY", false, false, null, null, false, uiController.GetRightUnitUI(), false, null, TextColour.Negative, energyDownEffect, target.FXSpawner());
					CheckIfStunned(target);

					//if node is in boss's range
					if (targetNode.IsBossRangeNode || unitNode.IsBossRangeNode)
					{
						//increase notoriety more
						unit.Stats.Notoriety += Mathf.Min(ability.ability.SuccessNotoriety() * bossMultiplier, 10 - unit.Stats.Notoriety);
						CheckNotoriety(unit);
						uiController.AddToTextList((ability.ability.SuccessNotoriety() * bossMultiplier).ToString() + " NOTORIETY", false, false, null, null, true, uiController.GetLeftUnitUI(), false, null, TextColour.Negative, null, null);

						//decrease candidates's boss standing directly
						candidate.Stats.PromotionChance -= Mathf.Min(unit.Stats.Persuasion * bossMultiplier, candidate.Stats.PromotionChance);
					}
					else
					{
						//increase notoriety slightly
						unit.Stats.Notoriety += Mathf.Min(ability.ability.SuccessNotoriety(), 10 - unit.Stats.Notoriety);
						CheckNotoriety(unit);
						uiController.AddToTextList(ability.ability.SuccessNotoriety().ToString() + " NOTORIETY", false, false, null, null, true, uiController.GetLeftUnitUI(), false, null, TextColour.Negative, null, null);
					}
				}
				else
				{
					//if failure
					//show fail text above attacker
					uiController.AddToTextList("COUNTER!", true, false, target, null, false, null, false, null, TextColour.Negative, ability.ability.FailureEffect(), target.FXSpawner());

					//reduce target energy by amount
					unit.Stats.Energy -= Mathf.Min(EnergyReductionAmount(ability.ability.GetAmount(), target.Stats.SelfEsteem, unit.Stats.Persuasion, target.Stats.Energy), unit.Stats.Energy);
					uiController.AddToTextList("-" + EnergyReductionAmount(ability.ability.GetAmount(), target.Stats.SelfEsteem, unit.Stats.Persuasion, target.Stats.Energy).ToString() + " ENERGY", false, false, null, null, true, uiController.GetLeftUnitUI(), false, null, TextColour.Negative, energyDownEffect, unit.FXSpawner());
					CheckIfStunned(unit);

					//if node is in boss's range
					if (targetNode.IsBossRangeNode || unitNode.IsBossRangeNode)
					{
						//increase notoriety more
						unit.Stats.Notoriety += Mathf.Min(ability.ability.FailureNotoriety() * bossMultiplier, 10 - unit.Stats.Notoriety);
						CheckNotoriety(unit);
						uiController.AddToTextList((ability.ability.FailureNotoriety() * bossMultiplier).ToString() + " NOTORIETY", false, false, null, null, true, uiController.GetLeftUnitUI(), false, null, TextColour.Negative, null, null);

						//decrease unit's boss standing directly
						candidate.Stats.PromotionChance -= Mathf.Min(unit.Stats.Persuasion * bossMultiplier, candidate.Stats.PromotionChance);
					}
					else
					{
						//increase notoriety slightly
						unit.Stats.Notoriety += Mathf.Min(ability.ability.SuccessNotoriety(), 10 - unit.Stats.Notoriety);
						CheckNotoriety(unit);
						uiController.AddToTextList((ability.ability.SuccessNotoriety() * bossMultiplier).ToString() + " NOTORIETY", false, false, null, null, true, uiController.GetLeftUnitUI(), false, null, TextColour.Negative, null, null);
					}
				}

				//wipe nodes
				grid.WipeNodes();

				//show UI Text
				StartCoroutine(uiController.ShowDamageText(unit, targetNode.Occupant, targetNode.GroupOccupant));

				#endregion
				break;

			case "Encourage":
				#region
				//can only happen to unit
				//get target unit
				target = targetNode.Occupant;
				target.SetTalkingGraphic(true);
				candidate = null;

				//find the candidate this unit represents
				if (unit.Stats.Candidate)
				{
					candidate = unit;
				}
				else
				{
					Unit[] units = FindObjectsOfType<Unit>();
					foreach (Unit possibleCandidate in units)
					{
						if (possibleCandidate.Stats.GetUnitStats() == unit.Stats.ChosenCandidate)
						{
							candidate = possibleCandidate;
							break;
						}
					}
				}

				//turn target to face unit
				target.TurnUnitToFaceTarget(unitNode);

				//always sucessful
				uiController.AddToTextList("SUCCESS", true, false, unit, null, false, null, false, null, TextColour.Positive, ability.ability.SuccessEffect(), unit.FXSpawner());

				//increase target energy by amount
				target.Stats.Energy += Mathf.Min(EnergyAdditionAmount(unit.Stats.Persuasion, ability.ability.GetAmount()), 100 - target.Stats.Energy);
				uiController.AddToTextList(Mathf.Min(EnergyAdditionAmount(unit.Stats.Persuasion, ability.ability.GetAmount()), 100 - target.Stats.Energy).ToString() + " ENERGY", false, false, null, null, false, uiController.GetRightUnitUI(), false, null, TextColour.Positive, energyUpEffect, target.FXSpawner());
				CheckIfStunned(target);

				//if node is in boss's range
				if (targetNode.IsBossRangeNode || unitNode.IsBossRangeNode)
				{
					//decrease notoriety more
					unit.Stats.Notoriety -= Mathf.Min(ability.ability.SuccessNotoriety() * bossMultiplier, unit.Stats.Notoriety);
					CheckNotoriety(unit);
					uiController.AddToTextList("-" + (ability.ability.SuccessNotoriety() * bossMultiplier).ToString() + " NOTORIETY", false, false, null, null, true, uiController.GetLeftUnitUI(), false, null, TextColour.Positive, null, null);
				}
				else
				{
					//decrease notoriety slightly
					unit.Stats.Notoriety -= Mathf.Min(ability.ability.SuccessNotoriety(), unit.Stats.Notoriety); ;
					CheckNotoriety(unit);
					uiController.AddToTextList("-" + ability.ability.SuccessNotoriety().ToString() + " NOTORIETY", false, false, null, null, true, uiController.GetLeftUnitUI(), false, null, TextColour.Positive, null, null);
				}

				//wipe nodes
				grid.WipeNodes();

				//show UI Text
				StartCoroutine(uiController.ShowDamageText(unit, targetNode.Occupant, targetNode.GroupOccupant));
				#endregion
				break;

			case "Check In":
				#region
				targetGroup = GetComponent<UnitController>().GetGroupList()[unit.BossGroupIndex];
				targetGroup.SetTalkingGraphic(true);
				targetGroup.SetNPCRotation(targetGroup.FindDirection(unit));
				targetGroup.SetNPCAnimation(false);

				//increment boss's group index
				if (unit.BossGroupIndex + 1 < GetComponent<UnitController>().GetGroupList().Count)
				{
					unit.BossGroupIndex += 1;
				}
				else
				{
					unit.BossGroupIndex = 0;
				}


				//foreach through each candidate stats info
				foreach (CandidateStats stats in targetGroup.GetStatsList())
				{
					foreach (Unit candidateUnit in GetComponent<UnitController>().GetList(GamePhase.Candidate))
					{
						if (candidateUnit == stats.candidate)
						{
							candidateUnit.Stats.PromotionChance += Mathf.Min(stats.modifier, 100 - candidateUnit.Stats.PromotionChance);

							string text;

							if (stats.modifier >= 0)
							{
								text = stats.candidate.Stats.Name + " " + stats.modifier.ToString();
							}
							else
							{
								text = stats.candidate.Stats.Name + " - " + stats.modifier.ToString();
							}

							//add unit text to text list
							uiController.AddToTextList(text, false, false, null, null, true, null, true, null, TextColour.Positive, null, null);
						}
					}
				}

				targetGroup.WipeModifiers();

				//show UI Text
				StartCoroutine(uiController.ShowDamageText(unit, targetNode.Occupant, targetNode.GroupOccupant));
				#endregion
				break;
		}
	}

	public bool CheckSuccess(int defendersWit, int abilityDifficulty)
	{
		setNumber -= (Mathf.FloorToInt(defendersWit / 2) + abilityDifficulty);
		int generatedNumber = Random.Range(1, setNumber);
		return (generatedNumber >= targetNumber);
	}

	public float EnergyReductionAmount(int abilityAmount, int targetsSelfEsteem, int unitsPersuasion, float targetsCurrentEnergy)
	{
		return Mathf.Min(abilityAmount + (unitsPersuasion * 2) - (targetsSelfEsteem * 2), targetsCurrentEnergy);
	}

	public int EnergyAdditionAmount(int unitPersuasion, int abilityAmount)
	{
		return (unitPersuasion * 2) + abilityAmount;
	}

	private void CheckIfStunned(Unit target)
	{
		if (target.Stats.Energy > 0)
		{
			target.Stats.StunnedDuration = 0;
			target.Stats.IsStunned = false;
		}
		else
		{
			target.Stats.StunnedDuration = 1;
			target.Stats.IsStunned = true;
			uiController.AddToTextList("STUNNED", true, false, target, null, false, null, false, null, TextColour.Negative, stunnedEffect, target.FXSpawner());
		}
	}

	private void CheckNotoriety(Unit target)
	{
		if (target.Stats.Notoriety >= 10)
		{
			//add text to list
			target.Stats.NotorietyMaxed = true;
		}
	}
}
