﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using UnityEngine;

public class UnitController : MonoBehaviour
{
	List<Unit> blueTeamList = new List<Unit>();
	List<Unit> redTeamList = new List<Unit>();
	List<Unit> yellowTeamList = new List<Unit>();
	List<Unit> candidateList = new List<Unit>();
	List<Group> groupList = new List<Group>();
	
	Unit bossUnit;
	public int currentUnitIndex;

	public void FillUnitList()
	{
		//Get all units in the scene
		Unit[] foundUnits = FindObjectsOfType<Unit>();

		//Add them all to list
		foreach (Unit unit in foundUnits)
		{
			//add unit to grid
			unit.AttachToGrid();

			if (unit.Stats.Candidate)
			{
				candidateList.Add(unit);
			}

			switch (unit.Stats.Team)
			{
				case GamePhase.BlueTeam:
					blueTeamList.Add(unit);
					break;

				case GamePhase.RedTeam:
					redTeamList.Add(unit);
					break;

				case GamePhase.YellowTeam:
					yellowTeamList.Add(unit);
					break;

				case GamePhase.Boss:
					bossUnit = unit;
					break;
			}
		}

		FillGroups();
	}

	private void FillGroups()
	{
		Group[] groups = FindObjectsOfType<Group>();
		foreach (Group group in groups)
		{
			group.AttachToGrid();
			group.InitializeCandidateStats(candidateList);
			groupList.Add(group);
		}
	}

	public void SetUnitActions(GamePhase phase)
	{
		switch(phase)
		{
			case GamePhase.BlueTeam:
				foreach (Unit unit in blueTeamList)
				{
					unit.SetActions();
				}
				break;

			case GamePhase.RedTeam:
				foreach (Unit unit in redTeamList)
				{
					unit.SetActions();
				}
				break;

			case GamePhase.YellowTeam:
				foreach (Unit unit in yellowTeamList)
				{
					unit.SetActions();
				}
				break;

			case GamePhase.Boss:
				bossUnit.SetActions();
				break;
		}
	}

	public List<Unit> GetList(GamePhase phase)
	{
		switch(phase)
		{
			case GamePhase.BlueTeam:
				return blueTeamList;

			case GamePhase.RedTeam:
				return redTeamList;

			case GamePhase.YellowTeam:
				return yellowTeamList;

			case GamePhase.Candidate:
				return candidateList;

		}

		return null;
	}

	public List<Group> GetGroupList()
	{
		return groupList;
	}

	public Unit GetBoss()
	{
		return bossUnit;
	}

	public void RemoveFromList(Unit unit)
	{
		switch (unit.Stats.Team)
		{
			case GamePhase.BlueTeam:
				blueTeamList.Remove(unit);
				break;

			case GamePhase.RedTeam:
				redTeamList.Remove(unit);
				break;

			case GamePhase.YellowTeam:
				yellowTeamList.Remove(unit);
				break;

		}
	}
}
