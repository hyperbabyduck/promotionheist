﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum InputState
{
    None,
    SelectUnit,
    SelectAction,
    Action,
    Move,
}

public class InputController : MonoBehaviour
{
    [SerializeField] UIController uiController;
    [SerializeField] BattleController battleController;
	[SerializeField] CameraMover cameraMover;
	[SerializeField] float cameraMoveSpeed = 2f;
    InputState inputState = InputState.None;

	Unit currentUnit;
	Group highlightedGroup;
    Node selectedNode;
	bool allowUIWipe = true;

	public void AllowUnitWipe(bool value)
	{
		allowUIWipe = value;
	}

	public InputState InputMode
    {
        get { return inputState; }
        set { inputState = value; }
    }

    public Unit CurrentUnit
    {
        get { return currentUnit; }
        set { currentUnit = value; }
    }

	private void Update()
	{
        if (InputMode != InputState.None)
		{
			InteractWithCamera();

			//if the mouse is over a node
			if (InteractWithNode())
			{
				//if the selected node is set
				if (selectedNode)
				{
					//set highlighted unit UI
					if (selectedNode.IsOccupied)
					{
						//if the highlighted unit is a group
						if (selectedNode.IsGroupNode)
						{
							highlightedGroup = selectedNode.GroupOccupant;
							uiController.SetHighlightedGroup(highlightedGroup);
						}
						else
						{
							if (selectedNode.Occupant.Stats.Team != GamePhase.Boss)
							{
								//if selecting unit
								if (InputMode == InputState.SelectUnit)
								{
									//set current displayed node
									currentUnit = selectedNode.Occupant;

									//set highlighted unit on the left hand side
									uiController.SetSelectedUnitStats(selectedNode.Occupant);
								}
								//if the player isn't selecting a character and thus the left character is already highlighted
								else if (InputMode != InputState.None)
								{
									//if the occupant isnt already the selected character which will be showing on the left
									if (selectedNode.Occupant != battleController.SelectedUnit)
									{
										//set current displayed node
										currentUnit = selectedNode.Occupant;

										//set the highlighted character on the right
										uiController.SetHighlightedUnitStats(selectedNode.Occupant);
									}
									else
									{
										WipeCurrentUnit();
									}
								}
							}
							else
							{
								//if the occupant isnt already the selected character which will be showing on the left
								if (selectedNode.Occupant != battleController.SelectedUnit)
								{
									if (battleController.SelectedUnit == null)
									{
										uiController.SetHighlightedBoss(selectedNode.Occupant, true);
									}
									else
									{
										uiController.SetHighlightedBoss(selectedNode.Occupant, false);
									}
								}
								else
								{
									WipeCurrentUnit();
								}
							}
						}
					}
					else
					{
						WipeCurrentUnit();
					}

					//if selected node is clicked on 
					if (Input.GetMouseButtonDown(0))
					{
						switch (InputMode)
						{
							case InputState.SelectUnit:
								if (!selectedNode.IsGroupNode)
								{
									//if the node is occupied and the occupant is on the player team
									if (selectedNode.IsOccupied && selectedNode.Occupant.Stats.Team == battleController.Phase)
									{
										if (selectedNode.Occupant.HasTakenTurn == false)
										{
											//set the selected unit in the battle controller
											battleController.SelectedUnit = selectedNode.Occupant;

											//start the unit's turn
											battleController.MoveCameraToUnitThenStartTurn(battleController.SelectedUnit);
										}
									}
								}
								break;

							case InputState.Move:
								//if the node is a move node
								if (selectedNode.IsMoveNode)
								{
									StartCoroutine(StartMove());
								}
								break;

							case InputState.Action:
								if(selectedNode.IsRangeNode)
								{
									if (battleController.SelectedUnit.SelectedAbility.ability.TargetSelf())
									{
										if (selectedNode.IsOccupied && selectedNode.Occupant == battleController.SelectedUnit)
										{
											StartAction();
										}
									}
									else 
									{
										foreach(GamePhase target in battleController.SelectedUnit.SelectedAbility.ability.GetTargets())
										{
											switch(target)
											{
												case GamePhase.Group:
													if (selectedNode.IsGroupNode)
													{
														StartAction();
													}
													break;

												case GamePhase.Boss:
													if (selectedNode.IsOccupied && !selectedNode.IsGroupNode)
													{
														if (selectedNode.Occupant.Stats.Team == GamePhase.Boss)
														{
															StartAction();
														}
													}
													break;

												default:
													if (selectedNode.IsOccupied && !selectedNode.IsGroupNode)
													{
														StartAction();
													}
													break;

											}
										}
									}
								}
								break;

							
						}
					}

					
				}

				return;
			}

			if (Input.GetKeyDown(KeyCode.Escape))
			{
				Unit currentUnit;
				GridController grid;

				switch (InputMode)
				{
					case InputState.Move:
						//find unit
						currentUnit = battleController.SelectedUnit;

						//wipe nodes
						grid = GameObject.FindGameObjectWithTag("GridController").GetComponent<GridController>();
						grid.WipeNodes();

						//show buttons
						uiController.SetPlayerButtons(true, currentUnit);

						//change input state
						InputMode = InputState.SelectAction;

						break;

					case InputState.Action:
						//wipe unit's selected action
						currentUnit = battleController.SelectedUnit;
						currentUnit.SelectedAbility = null;

						//wipe nodes
						grid = GameObject.FindGameObjectWithTag("GridController").GetComponent<GridController>();
						grid.WipeNodes();

						//show buttons
						uiController.SetPlayerButtons(true, currentUnit);

						//change input state
						InputMode = InputState.SelectAction;
						break;

				}
			}

			if (allowUIWipe)
			{
				//if not interacting with anything
				WipeCurrentUnit();
			}

			if (selectedNode)
			{
				SetNodeHighlighted(selectedNode, false);
				selectedNode = null;
			}
		}
	}

	private void WipeCurrentUnit()
	{
		//if there is a store unit, dont wipe unit
		if (highlightedGroup)
		{
			uiController.SetHighlightedGroup(null);
			uiController.SetHighlightedBoss(null, true);
			uiController.SetHighlightedBoss(null, false);
			highlightedGroup = null;
		}
		
		if (currentUnit)
		{
			if (battleController.SelectedUnit != null)
			{
				uiController.SetHighlightedUnitStats(null);

				if (battleController.SelectedUnit.Stats.Team != GamePhase.Boss)
				{
					uiController.SetHighlightedBoss(null, true);
					uiController.SetHighlightedBoss(null, false);
				}	
			}
			else
			{
				//if the occupant isnt already the selected character which will be showing on the left
				if (currentUnit != battleController.SelectedUnit)
				{
					uiController.SetHighlightedUnitStats(null);

					//set the highlighted character on the right
					uiController.SetSelectedUnitStats(null);
				}
				else
				{
					uiController.SetHighlightedUnitStats(null);

					uiController.SetHighlightedBoss(null, true);
					uiController.SetHighlightedBoss(null, false);
				}
			}

			//clear unit
			currentUnit = null;
		}
		else
		{
			if (battleController.SelectedUnit != null)
			{
				uiController.SetHighlightedUnitStats(null);

				if (battleController.SelectedUnit.Stats.Team != GamePhase.Boss)
				{
					uiController.SetHighlightedBoss(null, true);
					uiController.SetHighlightedBoss(null, false);
				}
			}
			else
			{
				//set the highlighted character on the right
				uiController.SetSelectedUnitStats(null);

				uiController.SetHighlightedBoss(null, true);
				uiController.SetHighlightedBoss(null, false);

				uiController.SetHighlightedUnitStats(null);
			}
		}
	}

    private bool InteractWithNode()
    {
        //get all raycast hits and sort them into a list
        RaycastHit[] hits = RaycastAllSorted();

        //go thorough each of the hits
        foreach (RaycastHit hit in hits)
        {
            //get the raycastable component from the object hit
            IRaycastable raycastableComponent = hit.transform.GetComponent<IRaycastable>();

            //if the object is hit by the raycast and active
            if (raycastableComponent.HandleRaycast())
            {
                Node node = hit.transform.GetComponent<Node>();

                //if component has a node component
                if (node)
				{
                    if (node != selectedNode)
					{
                        if(selectedNode)
						{
                            //wipe old node
                            SetNodeHighlighted(selectedNode, false);
                        }
                    }

                    //highlight new node
                    selectedNode = node;
                    SetNodeHighlighted(selectedNode, true);

                    return true;
                }
				else
				{
                    if (selectedNode)
					{
                        //wipe old node
                        SetNodeHighlighted(selectedNode, false);
                        selectedNode = null;
                    }
                }
            }
        }

        //wipe node if any is selected
        if (selectedNode)
        {
            //wipe old node
            SetNodeHighlighted(selectedNode, false);
            selectedNode = null;
        }
        return false;
    }

	private void InteractWithCamera()
	{
		float xAxisValue = Input.GetAxis("Horizontal") * cameraMoveSpeed * Time.deltaTime;
		float zAxisValue = Input.GetAxis("Vertical") * cameraMoveSpeed * Time.deltaTime;

		cameraMover.MoveCamera(new Vector3(cameraMover.transform.position.x + xAxisValue, cameraMover.transform.position.y, cameraMover.transform.position.z + zAxisValue));
	}

    private void SetNodeHighlighted(Node node, bool isTrue)
	{
        node.Highlighted = isTrue;
        node.ColourNode();
    }

    private RaycastHit[] RaycastAllSorted()
    {
        RaycastHit[] hits = Physics.RaycastAll(GetMouseRay());
        float[] distances = new float[hits.Length];

        for (int index = 0; index < hits.Length; index += 1)
        {
            distances[index] = hits[index].distance;
        }

        Array.Sort(distances, hits);

        return hits;
    }

    private Ray GetMouseRay()
    {
        return Camera.main.ScreenPointToRay(Input.mousePosition);
    }

	private IEnumerator StartMove()
	{
		if (cameraMover.transform.position != battleController.SelectedUnit.transform.position)
		{
			cameraMover.MoveCamera(battleController.SelectedUnit.transform.position);

			yield return new WaitForSeconds(cameraMover.MoveSpeed() / 2);
		}

		//stop input
		InputMode = InputState.None;

		//start the unit's movement
		battleController.SelectedUnit.StartMovement(selectedNode);
	}

	private void StartAction()
	{
		cameraMover.MoveCamera(battleController.SelectedUnit.transform.position);

		SetNodeHighlighted(selectedNode, false);

		//change input state
		InputMode = InputState.None;

		AllowUnitWipe(false);

		//perform Selected action
		battleController.SelectedUnit.PerformAction(selectedNode);
	}
}
