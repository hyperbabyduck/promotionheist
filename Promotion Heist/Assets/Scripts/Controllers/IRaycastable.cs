﻿public interface IRaycastable
{
	bool HandleRaycast();
}
