﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public enum NodeColour
{
    Move,
    Range,
    None
}

[System.Serializable]
public class NodeColours
{
    public Color moveNodeColour;
    public Color selectedMoveNodeColour;
    public Color rangeNodeColour;
    public Color selectedRangeNodeColour;
    public Color blueTeamColour;
    public Color blueHighlightedTeamColour;
    public Color redTeamColour;
    public Color redHighlightedTeamColour;
    public Color yellowTeamColour;
    public Color yellowHighlightedTeamColour;
    public Color bossOccupiedNodeColour;    
    public Color selectedBossOccupiedNodeColour;
    public Color groupNodeColour;
    public Color selectedGroupNodeColour;
    public Color bossRangeNodeColour;
}

public class GridController : MonoBehaviour
{
    [SerializeField] int bossRange = 4;
    [SerializeField] NodeColours nodeColours;
    Dictionary<Vector2Int, Node> grid = new Dictionary<Vector2Int, Node>();
    Queue<Node> queue = new Queue<Node>();
    Queue<Node> rangeQueue = new Queue<Node>();

    Node currentNode = null;
    List<Node> path = new List<Node>();

    Vector2Int[] directions =
    {
        Vector2Int.right,
        Vector2Int.up,
        Vector2Int.left,
        Vector2Int.down
    };

    //SETUP
    public void CreateGrid()
    {
        //find all of the Nodes
        Node[] nodes = FindObjectsOfType<Node>();

        //add their position to the dictionary
        foreach (Node node in nodes)
        {
            var gridPos = node.AttachToGrid();

            //if there are two Nodes at the same coordinates, skip the second one
            if (grid.ContainsKey(gridPos))
            {
                Debug.Log("Skipping overlapping block : " + node);
            }
            else
            {
                //add to grid
                grid.Add(gridPos, node);

                //set colours
                node.SetColourValues(nodeColours);
            }
        }
    }

    //MOVE RANGE
    public void FindMoveNodes(Unit currentUnit)
    {
        Node startingNode = grid[currentUnit.GridPos];

        //create path list
        queue = new Queue<Node>();

        BreadthFirstSearch(startingNode, currentUnit, false, true);

        //store start node for undo move purposes
        currentUnit.PreviousNode = startingNode;

        ColourNodes();
        
        //set input mode to allow input
        GameObject.FindGameObjectWithTag("InputController").GetComponent<InputController>().InputMode = InputState.Move;
    }

    public void FindRangeNodes(Unit currentUnit, Ability currentAbility)
	{
        currentNode = grid[currentUnit.GridPos];

        rangeQueue = new Queue<Node>();

        if (currentAbility.TargetSelf())
		{
            currentNode.IsRangeNode = true;
		}
		else
		{
            //add neighbours to queue
            SearchRangeNeighbours();
        }

        ColourNodes();

        //set input mode to allow input
        GameObject.FindGameObjectWithTag("InputController").GetComponent<InputController>().InputMode = InputState.Action;
    }

    //MOVEMENT
    public void FindAIMoveNodes(Unit currentUnit)
    {
        Node startingNode = grid[currentUnit.GridPos];

        //create path list
        queue = new Queue<Node>();

        BreadthFirstSearch(startingNode, currentUnit, true, true);
    }

    private void BreadthFirstSearch(Node startingNode, Unit currentUnit, bool isAI, bool setMoveNodes)
    { 
        //add the starting Node to the queue
        queue.Enqueue(startingNode);

        //while there are Nodes still to check and the end hasnt been found
        while (queue.Count > 0)
        {
            //remove current Node from queue
            currentNode = queue.Dequeue();

            //Add neighbours of the current Node to the queue
            ExploreNeighbours(currentUnit, isAI, setMoveNodes);

            //set the current waypooint as explored
            currentNode.IsExplored = true;
        }
    }

    private void ExploreNeighbours(Unit currentUnit, bool isAI, bool setMoveNodes)
    {
        //search for a neighbour in all 4 directions
        foreach (Vector2Int direction in directions)
        {
            //find the current neighbour to be checked
            Vector2Int neighbourCoordinates = currentNode.GridPos + direction;

            //if the neighbour Node exists
            if (grid.ContainsKey(neighbourCoordinates))
            {
                if (isAI)
				{
                    //add it to the queue
                    QueueNewAINeighbours(neighbourCoordinates, currentUnit, setMoveNodes);
                }
                else
				{
                    //add it to the queue
                    QueueNewNeighbours(neighbourCoordinates, currentUnit);
                }
            }
        }
    }

    private void QueueNewNeighbours(Vector2Int neighbourCoordinates, Unit currentUnit)
    {
        //get the neighbour Node at the coordinates
        Node neighbour = grid[neighbourCoordinates];

        //if the neighbour hasnt already been searched and isnt already in the queue
        if (!neighbour.IsExplored || queue.Contains(neighbour))
        {
            //if the neighbour is passable and within the unit's move range
            if ((currentNode.TotalMovementCost + neighbour.MovementCost) <= currentUnit.Stats.MoveRange)
            {
                //if the node isnt occupied, occupied by a knocked out unit or occupied by a teammate
                if (!neighbour.IsOccupied)
                {
                    //add it to the queue and set where it was added from
                    queue.Enqueue(neighbour);
                    neighbour.ExploredFrom = currentNode;

                    //give the node a movement cost
                    neighbour.TotalMovementCost = currentNode.TotalMovementCost + neighbour.MovementCost;
                    neighbour.IsMoveNode = true;
                }
            }
        }
    }

    //RANGE
    private void SearchRangeNeighbours()
	{
        //search for a neighbour in all 4 directions
        foreach (Vector2Int direction in directions)
        {
            //find the current neighbour to be checked
            Vector2Int neighbourCoordinates = currentNode.GridPos + direction;

            //if the neighbour Node exists
            if (grid.ContainsKey(neighbourCoordinates))
            {
                //check if it can be added to the queue
                QueueRangeNodes(neighbourCoordinates);
            }
        }
    }

    private void QueueRangeNodes(Vector2Int nodeCoordinates)
	{
        //find node
        Node neighbour = grid[nodeCoordinates];

        neighbour.IsRangeNode = true;
        rangeQueue.Enqueue(neighbour);
	}

    //PATHFINDING
    public List<Node> GetPath(Node startingNode, Node targetNode)
    {
        if (startingNode == targetNode)
        {
            path.Add(startingNode);
        }
        else
        {
            CreatePath(startingNode, targetNode);
        }

        return path;
    }

    public void CreatePath(Node startingNode, Node targetNode)
    {        
        path.Clear();

        //add the target Node to the queue
        SetAsPath(targetNode);

        Node previous = targetNode.exploredFrom;

        while (previous != startingNode)
		{
            SetAsPath(previous);
            previous = previous.exploredFrom;
        }

        //add the starting position to the end of the queue
        SetAsPath(startingNode);

        //reverse the queue so it is in order
        path.Reverse();
    }

    private void SetAsPath(Node Node)
    {
        path.Add(Node);
    }

    public void WipePath()
    {
        path.Clear();
        queue.Clear();
    }

    //NODES
    public Node GetNodeFromGrid(Vector2Int gridPos)
    {
        return grid[gridPos];
    }

    public void ColourNodes()
    {
        Node[] nodes = FindObjectsOfType<Node>();

        foreach (Node node in nodes)
        {
            node.ColourNode();
        }
    }

    public void WipeNodes()
    {
        foreach (Node node in grid.Values)
        {
            node.IsExplored = false;
            node.IsMoveNode = false;
            node.IsRangeNode = false;
            node.TotalMovementCost = 0;

            node.ColourNode();
        }
    }

    public void WipePathfindingInfo()
	{
        foreach (Node node in grid.Values)
        {
            node.IsExplored = false;
            node.ExploredFrom = null;
            node.TotalMovementCost = 0;
        }
    }

    public void WipeMoveNodes()
    {
        foreach (Node node in grid.Values)
        {
            node.IsMoveNode = false;

            node.ColourNode();
        }
    }

    public void ClearNodeColour()
	{
        foreach (Node node in grid.Values)
        {
            node.SetNodeColour(Color.white, false);
        }
    }

    public void FindBossRangeNodes(Unit boss)
	{
        Node startingNode = grid[boss.GridPos];

        //create path list
        queue = new Queue<Node>();

        BossNodeBreadthSearch(startingNode);

        WipeNodes(); //this will also colour them
    }

    private void BossNodeBreadthSearch(Node startingNode)
	{
        //add the starting Node to the queue
        queue.Enqueue(startingNode);

        //while there are Nodes still to check and the end hasnt been found
        while (queue.Count > 0)
        {
            //remove current Node from queue
            currentNode = queue.Dequeue();

            //Add neighbours of the current Node to the queue
            ExploreBossRangeNeighbours();

            //set the current waypooint as explored
            currentNode.IsExplored = true;
        }
    }

    private void ExploreBossRangeNeighbours()
    {
        //search for a neighbour in all 4 directions
        foreach (Vector2Int direction in directions)
        {
            //find the current neighbour to be checked
            Vector2Int neighbourCoordinates = currentNode.GridPos + direction;

            //if the neighbour Node exists
            if (grid.ContainsKey(neighbourCoordinates))
            {
                //add it to the queue
                QueueBossRangeNeighbours(neighbourCoordinates);
            }
        }
    }

    private void QueueBossRangeNeighbours(Vector2Int neighbourCoordinates)
	{
        //get the neighbour Node at the coordinates
        Node neighbour = grid[neighbourCoordinates];
        
        //if the neighbour hasnt already been searched and isnt already in the queue
        if (!neighbour.IsExplored || queue.Contains(neighbour))
        {
            //if the neighbour is passable and within the unit's move range
            if ((currentNode.TotalMovementCost + neighbour.MovementCost) <= bossRange)
            {
                //add it to the queue and set where it was added from
                queue.Enqueue(neighbour);
                neighbour.ExploredFrom = currentNode;

                //give the node a movement cost
                neighbour.TotalMovementCost = currentNode.TotalMovementCost + neighbour.MovementCost;

                neighbour.IsBossRangeNode = true;
            }
        }
    }

    public void WipeBossRangeNodes()
	{
        foreach (Node node in grid.Values)
        {
            SetBossRangeNode(node, false);
		}
	}

    private void SetBossRangeNode(Node node, bool value)
	{
        node.IsBossRangeNode = value;
        node.ColourNode();
	}

    //OCCUPATION
    public void OccupyGridPos(Unit unit, Vector2Int gridPos)
    {
        grid[gridPos].Occupant = unit;
        grid[gridPos].IsOccupied = true;
        grid[gridPos].ColourNode();
    }

    public void OccupyGroupGridPos(Group group, Vector2Int gridPos)
	{
        grid[gridPos].IsGroupNode = true;
        grid[gridPos].GroupOccupant = group;
        grid[gridPos].IsOccupied = true;
        grid[gridPos].ColourNode();
    }

    public void EmptyGridPos(Vector2Int gridPos)
    {
        grid[gridPos].Occupant = null;
        grid[gridPos].IsOccupied = false;
        grid[gridPos].ColourNode();
    }

    public Vector2Int[] GetDirections()
	{
        return directions;
	}

    //AI

    private void QueueNewAINeighbours(Vector2Int neighbourCoordinates, Unit currentUnit, bool setMoveNodes)
    {
        //get the neighbour Node at the coordinates
        Node neighbour = grid[neighbourCoordinates];

        //if the neighbour hasnt already been searched and isnt already in the queue
        if (!neighbour.IsExplored || queue.Contains(neighbour))
        {
            if (setMoveNodes)
			{
                //if the neighbour is passable and within the unit's move range
                if ((currentNode.TotalMovementCost + neighbour.MovementCost) <= currentUnit.Stats.MoveRange) 
                {
                    //currentUnit.Stats.MoveRange)
                    //if the node isnt occupied, occupied by a knocked out unit or occupied by a teammate
                    if (!neighbour.IsOccupied)
                    {  
                        if ((currentNode.TotalMovementCost + neighbour.MovementCost) <= currentUnit.Stats.MoveRange)
                        {
                            //add it to the queue and set where it was added from
                            queue.Enqueue(neighbour);
                            neighbour.ExploredFrom = currentNode;

                            //give the node a movement cost
                            neighbour.TotalMovementCost = currentNode.TotalMovementCost + neighbour.MovementCost;

                            currentUnit.GetComponent<AIController>().AddToMoveNodeList(neighbour);

                            neighbour.IsMoveNode = true;
                        }
                    }
                }
            }
        }
    }

    public bool GridContainsNode(Vector2Int coordinates)
	{
        return grid.ContainsKey(coordinates);
	}
}
