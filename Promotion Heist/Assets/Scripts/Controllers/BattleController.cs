﻿using Cinemachine;
using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public struct Global
{
	public const int GRID_SIZE = 1;
}

public enum GamePhase
{
	None,
	BlueTeam,
	RedTeam,
	YellowTeam,
	Boss,
	Group,
	Candidate
}

public class BattleController : MonoBehaviour
{
	[SerializeField] int maxRoundNumber = 20;
	[SerializeField] GridController gridController;
	[SerializeField] UIController uiController;
	[SerializeField] InputController inputController;
	[SerializeField] CameraMover cameraMover;
	[SerializeField] float fadeTime = 2f;

	Unit currentUnit;
	Unit removeUnit;
	GamePhase currentPhase = GamePhase.None;
	int roundNumber = 1;
	bool blueTeamAlive = true;
	bool redTeamAlive = true;
	bool yellowTeamAlive = true;

	private void Start()
	{
		//InitializeBattle();
		SelectedUnit = null;
	}

	public void StartOpeningSceneDialogue()
	{
		InitializeBattle();
	}

	public void InitializeBattle()
	{
		//create the grid
		gridController.CreateGrid();
		GetComponent<UnitController>().FillUnitList();

		uiController.SetCandidateUI();
		uiController.SetTurnLimitText(maxRoundNumber, roundNumber);

		//set initial boss range nodes
		gridController.FindBossRangeNodes(GetComponent<UnitController>().GetBoss());

		uiController.GetComponent<UIFader>().FadeIn(fadeTime);

		//set starting phase
		SetPhase();
	}

	public void SetPhase()
	{
		UnitController unitController = GetComponent<UnitController>();

		switch (currentPhase)
		{
			case GamePhase.None:
				//change phase
				Phase = GamePhase.BlueTeam;

				//set phase UI
				uiController.SetPhaseUI(currentPhase);

				GetComponent<UnitController>().SetUnitActions(currentPhase);

				//move camera to player
				foreach (Unit unit in unitController.GetList(Phase))
				{
					if (cameraMover.transform.position != unit.transform.position)
					{
						cameraMover.MoveCamera(unit.transform.position);
					}
					break;
				}

				inputController.InputMode = InputState.SelectUnit;

				break;

			case GamePhase.BlueTeam:
				//change phase
				Phase = GamePhase.RedTeam;

				//set phase UI
				uiController.SetPhaseUI(currentPhase);

				GetComponent<UnitController>().SetUnitActions(currentPhase);

				//move camera to player
				foreach (Unit unit in unitController.GetList(Phase))
				{
					if (cameraMover.transform.position != unit.transform.position)
					{
						cameraMover.MoveCamera(unit.transform.position);
					}
					break;
				}

				inputController.InputMode = InputState.SelectUnit;
				break;

			case GamePhase.RedTeam:

				//change phase
				Phase = GamePhase.YellowTeam;

				//set phase UI
				uiController.SetPhaseUI(currentPhase);

				GetComponent<UnitController>().SetUnitActions(currentPhase);

				//move camera to player
				foreach (Unit unit in unitController.GetList(Phase))
				{
					if (cameraMover.transform.position != unit.transform.position)
					{
						cameraMover.MoveCamera(unit.transform.position);
					}
					break;
				}

				inputController.InputMode = InputState.SelectUnit;

				break;


			case GamePhase.YellowTeam:

				SelectedUnit = unitController.GetBoss();

				Phase = GamePhase.Boss;

				//set phase UI
				uiController.SetPhaseUI(currentPhase);

				//find current unit
				unitController.SetUnitActions(currentPhase);

				MoveCameraToUnitThenStartTurn(SelectedUnit);

				break;

			case GamePhase.Boss:
				//increment round
				IncrementRound(unitController);
				break;
		}
	}

	public GamePhase Phase
	{
		get { return currentPhase; }
		set { currentPhase = value; }
	}

	private void IncrementRound(UnitController unitController)
	{
		if (roundNumber + 1 > maxRoundNumber)
		{
			//END GAME
			EndGame(FindWinningTeam());
		}
		else
		{
			roundNumber += 1;

			//start player phase
			Phase = GamePhase.BlueTeam;

			uiController.SetTurnLimitText(maxRoundNumber, roundNumber);

			//set phase UI
			uiController.SetPhaseUI(currentPhase);

			GetComponent<UnitController>().SetUnitActions(currentPhase);

			//move camera to player
			foreach (Unit unit in unitController.GetList(Phase))
			{
				if (cameraMover.transform.position != unit.transform.position)
				{
					cameraMover.MoveCamera(unit.transform.position);
				}
				break;
			}

			inputController.InputMode = InputState.SelectUnit;
		}
	}

	public Unit SelectedUnit
	{
		get { return currentUnit; }
		set { currentUnit = value; }
	}

	public void CheckIfCurrentPhaseComplete()
	{
		bool phaseComplete = true;
		UnitController unitController = GetComponent<UnitController>();

		if (currentPhase != GamePhase.Boss)
		{
			foreach (Unit unit in unitController.GetList(currentPhase))
			{
				if (unit.HasTakenTurn == false)
				{
					phaseComplete = false;
					break;
				}
			}
		}


		if (phaseComplete)
		{
			if (Phase != GamePhase.Boss)
			{
				uiController.ClearEndTurnIcons(unitController, currentPhase);
			}

			//go to next phase
			SetPhase();
		}
		else
		{
			//if player phase, allow movement
			if (currentPhase == GamePhase.Boss)
			{
				//start boss's turn
				SelectedUnit = unitController.GetBoss();
				MoveCameraToUnitThenStartTurn(SelectedUnit);
			}
			else
			{
				//set input type
				inputController.InputMode = InputState.SelectUnit;
			}
		}
	}

	public int RoundNumber
	{
		get { return roundNumber; }
		set { roundNumber = value; }
	}

	public bool BlueTeamAlive
	{
		get { return blueTeamAlive; }
		set { blueTeamAlive = value; }
	}

	public bool RedTeamAlive
	{
		get { return redTeamAlive; }
		set { redTeamAlive = value; }
	}

	public bool YellowTeamAlive
	{
		get { return yellowTeamAlive; }
		set { yellowTeamAlive = value; }
	}

	public int NumberOfTeamsRemaining()
	{
		int number = 0;

		if (BlueTeamAlive)
		{
			number += 1;
		}
		if (RedTeamAlive)
		{
			number += 1;
		}
		if (YellowTeamAlive)
		{
			number += 1;
		}

		return number;
	}

	private bool TeamRemains(GamePhase team)
	{
		switch (team)
		{
			case GamePhase.BlueTeam:
				return BlueTeamAlive;

			case GamePhase.RedTeam:
				return RedTeamAlive;

			case GamePhase.YellowTeam:
				return YellowTeamAlive;
		}

		return false;
	}

	//change to coroutine
	public void BeginRemoveUnit(Unit unit)
	{
		//remove current unit
		SelectedUnit = null;
		removeUnit = unit;
		//remove unit
		StartCoroutine(RemoveUnitFade());
	}

	private IEnumerator RemoveUnitFade()
	{
		Fader fader = FindObjectOfType<Fader>();

		yield return fader.FadeOut(fadeTime);

		RemoveUnit();

		//wait//
		yield return new WaitForSeconds(fadeTime);

		//remove text
		uiController.SetUnitRemovedText(null);

		//unfade screen
		yield return fader.FadeIn(fadeTime);

		CheckForEndGame();
	}

	private void RemoveUnit()
	{
		//Disable prefab
		Destroy(removeUnit.GetComponentInChildren<SpawnedUnit>());

		//remove from grid
		gridController.EmptyGridPos(removeUnit.GridPos);

		//remove from list
		GetComponent<UnitController>().RemoveFromList(removeUnit);

		//move position
		removeUnit.transform.position = new Vector3(-5, 10, 0);

		//show text
		uiController.SetUnitRemovedText(removeUnit);
	}

	private void CheckForEndGame()
	{
		//if unit is the last one in their team
		if (GetComponent<UnitController>().GetList(removeUnit.Stats.Team).Count <= 0)
		{
			RemoveTeam(removeUnit.Stats.Team);
			removeUnit = null;

			//check if 2 teams remain
			if (NumberOfTeamsRemaining() < 2)
			{
				EndGame(FindWinningTeam());
			}
		}
		else
		{
			removeUnit = null;

			//start next turn
			CheckIfCurrentPhaseComplete();
		}
	}

	private GamePhase FindWinningTeam()
	{
		GamePhase team = GamePhase.None;
		Unit otherUnit = null;

		foreach(Unit candidate in GetComponent<UnitController>().GetList(GamePhase.Candidate))
		{
			//Get Candidate Team
			GamePhase currentTeam = candidate.Stats.Team;

			//if the team is still in the game
			if (TeamRemains(currentTeam))
			{
				//if there is already a unit stored
				if (otherUnit != null)
				{
					if (candidate.Stats.PromotionChance > otherUnit.Stats.PromotionChance)
					{
						team = currentTeam;
					}
				}
				else
				{
					team = currentTeam;
				}
			}
		}

		return team;
	}

	private void RemoveTeam(GamePhase team)
	{
		switch (team)
		{
			case GamePhase.BlueTeam:
				BlueTeamAlive = false;
				break;

			case GamePhase.RedTeam:
				RedTeamAlive = false;
				break;

			case GamePhase.YellowTeam:
				YellowTeamAlive = false;
				break;
		}
	}

	public void EndGame(GamePhase phase)
	{
		uiController.SetEndScreen(phase);
	}

	public void MoveCameraToUnitThenStartTurn(Unit targetUnit)
	{
		if (cameraMover.transform.position != targetUnit.transform.position)
		{
			cameraMover.MoveCamera(targetUnit.transform.position);
		}

		if (targetUnit.Stats.Team != GamePhase.Boss)
		{
			inputController.InputMode = InputState.SelectAction;
			SelectedUnit.StartTurn();
		}
		else
		{
			uiController.SetHighlightedBoss(SelectedUnit, true);
			SelectedUnit.StartTurn();
		}
	}
}
