﻿using Ricimi;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TextColour
{
	Neutral,
	Positive,
	Negative
}

public class UIController : MonoBehaviour
{
	[SerializeField] CameraMover cameraMover;
	[SerializeField] GameObject playerOptions;
	[SerializeField] DisplayUnitStats selectedUnitStats;
	[SerializeField] DisplayUnitStats highlightedUnitStats;
	[SerializeField] GroupText highlightedGroupStats;
	[SerializeField] BossUnitStats highlightedLeftBossStats;
	[SerializeField] BossUnitStats highlightedRightBossStats;
	[SerializeField] CandidateUIDisplayer candidateUI;
	[SerializeField] DisplayGamePhase gamePhaseUI;
	[SerializeField] DisplayTurnLimit turnLimitUI;
	[SerializeField] GameObject optionsUI;
	[SerializeField] EndScreen endScreen;
	[SerializeField] Text unitRemovedText;
	[SerializeField] Button moveButton;
	[SerializeField] Button endButton;
	[SerializeField] ActionButton[] actionButtons;
	[SerializeField] Color neutralColour;
	[SerializeField] Color positiveColour;
	[SerializeField] Color negativeColour;
	[SerializeField] float displayTextTime = 1f;

	List<TextDetails> textList = new List<TextDetails>();
	List<GameObject> effectList = new List<GameObject>();

	public struct TextDetails
	{
		public string textString;
		public bool spawnOverUnit;
		public bool spawnOverGroup;
		public Unit targetUnit;
		public Group targetGroup;
		public bool spawnLeft;
		public DisplayUnitStats unitUI;
		public bool bossUI;
		public GroupText groupUI;
		public Color colour;
		public GameObject effect;
		public Transform effectSpawnPoint;
	}

	public void SetCandidateUI()
	{
		candidateUI.InitializeSlots();
	}

	public void SetPhaseUI(GamePhase phase)
	{
		gamePhaseUI.SetNewPhase(phase);
	}

	public void SetPlayerButtons(bool value, Unit currentUnit)
	{
		if (value)
		{
			CreatePlayerButtons(currentUnit);
		}
		
		playerOptions.SetActive(value);
	}

	private void CreatePlayerButtons(Unit currentUnit)
	{
		//create move
		moveButton.interactable = currentUnit.CanMove;
		moveButton.GetComponent<BasicButton>().SetUnit(currentUnit);
		if (currentUnit.CanMove)
		{
			moveButton.GetComponent<BasicButton>().ResetButton();
		}
		else
		{
			moveButton.GetComponent<BasicButton>().DisableButton();
		}
		

		//foreach through unit abilities
		for (int index = 0; index < currentUnit.Stats.Abilities.Count; index += 1)
		{


			actionButtons[index].GetComponent<BasicButton>().SetUnit(currentUnit);
			actionButtons[index].InitializeButton(currentUnit.Stats.Abilities[index]);
			ActivateButton(actionButtons[index], currentUnit.CanAct);
		}
		endButton.GetComponent<BasicButton>().ResetButton();
		endButton.GetComponent<BasicButton>().SetUnit(currentUnit);
	}

	private void ActivateButton(ActionButton button, bool value)
	{
		if (value)
		{
			button.GetComponent<BasicButton>().ResetButton();
		}
		else
		{
			button.GetComponent<BasicButton>().DisableButton();
		}

		button.GetComponent<Button>().interactable = value;
	}

	public void StartMoveAction()
	{
		Unit currentUnit = GameObject.FindGameObjectWithTag("BattleController").GetComponent<BattleController>().SelectedUnit;

		//hide buttons
		SetPlayerButtons(false, currentUnit);

		currentUnit.StartMoveAction();
	}

	public void StartAction(AbilityStats ability)
	{
		Unit currentUnit = GameObject.FindGameObjectWithTag("BattleController").GetComponent<BattleController>().SelectedUnit;
		//hide buttons
		SetPlayerButtons(false, currentUnit);
		currentUnit.SelectedAbility = ability;

		GridController grid = GameObject.FindGameObjectWithTag("GridController").GetComponent<GridController>();

		grid.FindRangeNodes(currentUnit, ability.ability);
	}

	public void EndTurn()
	{
		Unit currentUnit = GameObject.FindGameObjectWithTag("BattleController").GetComponent<BattleController>().SelectedUnit;

		//hide buttons
		SetPlayerButtons(false, currentUnit);
		currentUnit.EndTurn();
	}

	public DisplayUnitStats GetLeftUnitUI()
	{
		return selectedUnitStats;
	}

	public DisplayUnitStats GetRightUnitUI()
	{
		return highlightedUnitStats;
	}

	public GroupText GetGroupUI()
	{
		return highlightedGroupStats;
	}

	private Color GetColour(TextColour colour)
	{
		Color newColour = new Color(0,0,0);

		switch(colour)
		{
			case TextColour.Neutral:
				newColour = neutralColour;
				break;

			case TextColour.Positive:
				newColour = positiveColour;
				break;

			case TextColour.Negative:
				newColour = negativeColour;
				break;
		}

		return newColour;
	}

	public void SetSelectedUnitStats(Unit unit)
	{
		if (unit != null)
		{
			//set unit
			selectedUnitStats.CurrentUnit = unit;
			selectedUnitStats.gameObject.SetActive(true);
		}
		else
		{
			//wipe unit
			selectedUnitStats.CurrentUnit = null;
			selectedUnitStats.gameObject.SetActive(false);
		}
	}

	public void SetHighlightedUnitStats(Unit unit)
	{
		//if the unit isn't null or the current selected unit
		if (unit != null && unit != GameObject.FindGameObjectWithTag("BattleController").GetComponent<BattleController>().SelectedUnit)
		{
			//set unit
			highlightedUnitStats.CurrentUnit = unit;
			highlightedUnitStats.gameObject.SetActive(true);
		}
		else
		{
			//wipe unit
			highlightedUnitStats.CurrentUnit = null;
			highlightedUnitStats.gameObject.SetActive(false);
		}
	}

	public void SetHighlightedGroup(Group group)
	{
		if (group != null)
		{
			highlightedGroupStats.CurrentGroup = group;
			highlightedGroupStats.gameObject.SetActive(true);
		}
		else
		{
			//wipe unit
			highlightedGroupStats.CurrentGroup = null;
			highlightedGroupStats.gameObject.SetActive(false);
		}
	}

	public void SetHighlightedBoss(Unit unit, bool isLeft)
	{
		if (unit != null)
		{
			if(isLeft)
			{
				highlightedLeftBossStats.CurrentUnit = unit;
				highlightedLeftBossStats.gameObject.SetActive(true);
			}
			else
			{
				highlightedRightBossStats.CurrentUnit = unit;
				highlightedRightBossStats.gameObject.SetActive(true);
			}
			
		}
		else
		{
			//wipe unit
			highlightedLeftBossStats.CurrentUnit = null;
			highlightedLeftBossStats.gameObject.SetActive(false);

			highlightedRightBossStats.CurrentUnit = null;
			highlightedRightBossStats.gameObject.SetActive(false);
		}
	}

	public void AddToTextList(string textString, bool spawnOverUnit, bool spawnOverGroup, Unit targetUnit, Group targetGroup, bool spawnLeft, DisplayUnitStats unitUI, bool bossUI, GroupText group, TextColour colour, GameObject effect, Transform effectSpawn)
	{
		TextDetails text = new TextDetails();
		text.textString = textString;
		text.spawnOverUnit = spawnOverUnit;
		text.spawnOverGroup = spawnOverGroup;
		text.targetUnit = targetUnit;
		text.targetGroup = targetGroup;
		text.spawnLeft = spawnLeft;
		text.unitUI = unitUI;
		text.bossUI = bossUI;
		text.groupUI = group;
		text.colour = GetColour(colour);
		text.effect = effect;
		text.effectSpawnPoint = effectSpawn;

		textList.Add(text);
	}

	public IEnumerator ShowDamageText(Unit currentUnit, Unit target, Group targetGroup)
	{
		print(textList.Count);
		int leftIndex = 0;
		int rightIndex = 0;
		int index = 0;

		while (textList.Count > 0)
		{
			//remove text from list 
			TextDetails text = textList[0];
			textList.RemoveAt(0);

			if (text.bossUI)
			{
				highlightedLeftBossStats.ShowText(text.textString, leftIndex, text.colour);
				leftIndex += 1;
			}
			else
			{
				if (text.effect != null)
				{
					Instantiate(text.effect, text.effectSpawnPoint);

				}

				if (text.spawnLeft)
				{
					index = leftIndex;
				}
				else
				{
					index = rightIndex;
				}

				if (text.spawnOverUnit)
				{
					text.targetUnit.SpawnText(text.textString, text.colour);
				}
				else
				{
					if (text.unitUI != null)
					{
						text.unitUI.ShowText(text.textString, index, text.colour);
					}
					else if (text.groupUI != null)
					{
						text.groupUI.ShowText(text.textString, index, text.colour);
					}

					if (text.spawnLeft)
					{
						leftIndex += 1;
					}
					else
					{
						rightIndex += 1;
					}
				}
			}
			

			//show text
			yield return new WaitForSeconds(displayTextTime);
		}

		highlightedUnitStats.UpdateStats();
		selectedUnitStats.UpdateStats();

		CheckIfStunned(currentUnit);

		yield return new WaitForSeconds(displayTextTime * 2);

		GameObject.FindGameObjectWithTag("InputController").GetComponent<InputController>().AllowUnitWipe(true);

		if (highlightedGroupStats.CurrentGroup != null)
		{
			highlightedGroupStats.CurrentGroup.SetNPCRotation(highlightedGroupStats.CurrentGroup.StartingRotation);
			highlightedGroupStats.CurrentGroup.SetNPCAnimation(true);
		}

		//hide all text
		HideAllText();

		currentUnit.SetTalkingGraphic(false);

		if (target)
		{
			target.SetTalkingGraphic(false);
		}
		if (targetGroup)
		{
			targetGroup.SetTalkingGraphic(false);
		}

		currentUnit.EndAction();
	}

	private void CheckIfStunned(Unit currentUnit)
	{
		if (highlightedUnitStats.CurrentUnit != null)
		{
			if (highlightedUnitStats.CurrentUnit.Stats.IsStunned)
			{
				highlightedUnitStats.CurrentUnit.SetStunnedIcon(true);
			}
			else
			{
				highlightedUnitStats.CurrentUnit.SetStunnedIcon(false);
			}
		}

		if (currentUnit.Stats.IsStunned)
		{
			currentUnit.SetStunnedIcon(true);
		}
		else
		{
			currentUnit.SetStunnedIcon(false);
		}
	}

	private void HideAllText()
	{
		highlightedGroupStats.WipeText();
		highlightedUnitStats.WipeText();
		highlightedRightBossStats.WipeText();
		highlightedLeftBossStats.WipeText();
		selectedUnitStats.WipeText();

		SetHighlightedBoss(null, true);
		SetHighlightedBoss(null, false);
		SetHighlightedUnitStats(null);
		SetHighlightedGroup(null);
	}

	public void ShowEndTurnIcon(Unit unit)
	{
		unit.SetEndTurnIcon(true);
	}

	public void ClearEndTurnIcons(UnitController unitController, GamePhase team)
	{
		foreach (Unit unit in unitController.GetList(team))
		{
			unit.SetEndTurnIcon(false);
		}
	}

	public void SetTurnLimitText(int maxNumber, int number)
	{
		turnLimitUI.SetMaxRoundValue(maxNumber);
		turnLimitUI.SetRoundValue(number);
	}

	public DisplayUnitStats GetHighlightedUnitUI()
	{
		return highlightedUnitStats;
	}

	public void SetUnitRemovedText(Unit unit)
	{
		if (unit != null)
		{
			unitRemovedText.text = (unit.Stats.Name + " has been removed from the party!");
		}
		else
		{
			unitRemovedText.text = ("");
		}
	}

	public void SetEndScreen(GamePhase team)
	{
		endScreen.SetText(team);
	}
}
