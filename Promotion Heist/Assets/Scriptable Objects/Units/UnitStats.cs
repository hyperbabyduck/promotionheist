﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "NewUnitStats", menuName = "Units/Create New Unit Stats")]
public class UnitStats : ScriptableObject
{
	[SerializeField] string unitName = "error";
	[SerializeField] GamePhase team;
	[SerializeField] GameObject unitPrefab;
	[SerializeField] Sprite unitPicture;
	[SerializeField] Sprite unitHeadshot;
	[SerializeField] RuntimeAnimatorController animationController;
	[SerializeField] bool isCandidate = false;
	[SerializeField] UnitStats chosenCandidate;
	[Range(0, 100)]
	[SerializeField] float startingPromotionChance;
	[Range(0,10)]
	[SerializeField] int startingWit = 0;
	[Range(0, 10)]
	[SerializeField] int startingSelfEsteem = 0;
	[Range(0, 10)]
	[SerializeField] int startingPersuasion = 0;
	[Range(0, 100)]
	[SerializeField] float energy;
	[SerializeField] int moveRange = 0;

	[SerializeField] Ability[] abilities;

	public string GetName()
	{
		return unitName;
	}

	public GamePhase GetTeam()
	{
		return team;
	}

	public GameObject GetPrefab()
	{
		return unitPrefab;
	}

	public Sprite GetPicture()
	{
		return unitPicture;
	}

	public Sprite GetHeadshot()
	{
		return unitHeadshot;
	}

	public RuntimeAnimatorController GetAnimator()
	{
		return animationController;
	}

	public int GetStartingWit()
	{
		return startingWit;
	}

	public int GetStartingSelfEsteem()
	{
		return startingSelfEsteem;
	}

	public int GetStartingPersuasion()
	{
		return startingPersuasion;
	}

	public float GetEnergy()
	{
		return energy;
	}

	public int GetMoveRange()
	{
		return moveRange;
	}

	public bool GetIsCandidate()
	{
		return isCandidate;
	}

	public UnitStats GetChosenCandidate()
	{
		return chosenCandidate;
	}

	public float GetStartingPromotionChance()
	{
		return startingPromotionChance;
	}

	public Ability[] GetAbilities()
	{
		return abilities;
	}
}
