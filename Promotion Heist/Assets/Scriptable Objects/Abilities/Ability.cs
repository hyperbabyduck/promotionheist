﻿using Boo.Lang.Environments;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

[CreateAssetMenu(fileName = "NewAbility", menuName = "Abilities/New Ability")]
public class Ability : ScriptableObject
{
	[SerializeField] string abilityName = "error";
	[SerializeField] bool isBuff;
	[SerializeField] bool isDebuff;
	[SerializeField] PlayerStat affectedStat;
	[Range(0, 10)]
	[SerializeField] int amount;
	[SerializeField] bool isDistraction;
	[SerializeField] int distractionRange;
	[SerializeField] bool targetSelf;
	[SerializeField] GamePhase[] targets;
	[SerializeField] bool myTeam;
	[SerializeField] AnimationType animation;
	[Range(1, 3)]
	[SerializeField] int difficulty;
	[Range(0, 3)]
	[SerializeField] int successNotoriety;
	[Range(0, 3)]
	[SerializeField] int failureNotoriety;
	[Range(0, 5)]
	[SerializeField] int cooldown = 3;

	[Header("Effects")]
	[SerializeField] GameObject successEffect;
	[SerializeField] GameObject failureEffect;


	public string GetName()
	{
		return abilityName;
	}

	public bool IsBuff()
	{
		return isBuff;
	}

	public bool IsDebuff()
	{
		return isDebuff;
	}

	public PlayerStat AffectedStat()
	{
		return affectedStat;
	}

	public int GetAmount()
	{
		return amount;
	}

	public AnimationType GetAnimation()
	{
		return animation;
	}

	public bool IsDistraction()
	{
		return isDistraction;
	}

	public int DistractionRange()
	{
		return distractionRange;
	}

	public bool TargetSelf()
	{
		return targetSelf;
	}

	public GamePhase[] GetTargets()
	{
		return targets;
	}

	public bool MyTeam()
	{
		return myTeam;
	}

	public int Difficulty
	{
		get { return difficulty; }
		set { difficulty = value; }
	}

	public int SuccessNotoriety()
	{
		return successNotoriety;
	}

	public int FailureNotoriety()
	{
		return failureNotoriety;
	}

	public int Cooldown()
	{
		return cooldown;
	}

	public GameObject SuccessEffect()
	{
		return successEffect;
	}

	public GameObject FailureEffect()
	{
		return failureEffect;
	}

}
