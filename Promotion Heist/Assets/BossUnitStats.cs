﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossUnitStats : MonoBehaviour
{
	[SerializeField] Image unitPicture;
	[SerializeField] Text nameText;
	[SerializeField] Text[] textArray;
	[SerializeField] Text[] textBackgroundArray;

	Unit currentUnit;

	public Unit CurrentUnit
	{
		get { return currentUnit; }
		set
		{
			currentUnit = value;
			WipeUnitDetails();
			SetCurrentUnit();
		}
	}

	public void ShowStats(bool value)
	{
		gameObject.SetActive(value);
	}

	private void SetCurrentUnit()
	{
		if (CurrentUnit != null)
		{
			unitPicture.sprite = CurrentUnit.Stats.Picture;
			unitPicture.enabled = true;
			nameText.text = CurrentUnit.Stats.Name;
		}
		else
		{
			WipeUnitDetails();
		}
	}

	private void WipeUnitDetails()
	{
		unitPicture.enabled = false;
		nameText.text = "";
	}

	public void ShowText(string value, int index, Color colour)
	{
		textArray[index].color = colour;
		textArray[index].text = value;
		textBackgroundArray[index].text = value;
	}

	public void WipeText()
	{
		foreach (Text text in textArray)
		{
			text.color = Color.white;
			text.text = " ";
		}

		foreach (Text text in textBackgroundArray)
		{
			text.text = " ";
		}
	}
}
